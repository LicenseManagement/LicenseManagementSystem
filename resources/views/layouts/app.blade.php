<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- <link href="{{ asset('css/style.css') }}" rel="stylesheet"> -->

       <link href="{{ asset('styles/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('styles/js/jquery-ui-1.12.1/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ asset('styles/css/bootstrap-theme.min.css') }}" rel="stylesheet"
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app"> 
        @yield('content')
    </div>
    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
    <!-- Latest compiled and minified JavaScript -->
    <script type="text/javascript" src="{{ asset('styles/js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('styles/js/jquery-ui-1.12.1/jquery-ui.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('styles/js/bootstrap.min.js') }}"></script>

        @yield('script')
</body>
</html>
