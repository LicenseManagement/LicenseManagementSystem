@extends('layouts.app')
@section('content')
@include('header') {{-- Include header file --}} 

<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<div class="container">

    <div class="row">
        
            <div class="panel panel-default">
                <div class="panel-heading">List all Nationality</div>

                <div class="panel-body">
                   
                        <button type="button" class="btn btn-info" data-toggle="collapse" 
                            data-target="#demo">Add Nationality</button>

                        <form action="{{route('national.stored')}}" method="POST">
                      {!! csrf_field() !!}

                            <div class="col-xs-6">
                                <div id="demo" class="collapse" class="form-group">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">National</label>
                                        <input type="text" name="nation" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                                    </div>
                                  
                                    <div class="form-group">
                                        <label for="exampleTextarea">Information</label>
                                        <textarea class="form-control" id="exampleTextarea" name="descrip" rows="3"></textarea>
                                    </div>
                                 
                                        <button type="submit" class="btn btn-primary">Save</button>
                                        <button type="submit" class="btn btn-primary">Cancel</button>
                                </div>
                            </div>
                        </form>

                          <form action="{{URL::to('/national/search_national')}}" method="POST" class="navbar-form navbar-left">
                              <div class="form-group">
                                <label for="exampleInputEmail1">Search Nationality:</label>
                                <input type="text" name="search_nation" class="form-control" placeholder="Search national....">

                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                              </div>
                              <button type="submit" name="submit" class="btn btn-default">Go</button>
                        </form>

                      <div class="col-lg-12">
                         
                        <div class="box-container-toggle">
                          <div class="box-content">

                            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered bootstrap-datatable" id="datatable">
                            <tbody>
                              <tr>
                                <th>id</th>

                                <th>National</th>
                                
                                <th>Description</th>
                                <th>Action</th>
                               
                              </tr>


                              <?php
                                  foreach ($tbl_national as $national) {
                               ?>
                                 <td class="center"><?php echo $national->national_id;?></td>
                                 <td class="center"><?php echo $national->national;?></td>
                                 <td class="center"><?php echo $national->description;?></td>
                                
                                 <td class="center">
                              
                                    <form action="{{url('/delete_national',$national->national_id)}}" method="POST">
                                      <button type="submit" class="btn btn-danger pull-right">
                                        {!! csrf_field() !!}
                                        <i class="icon-trash icon-white"></i>  
                                        Delete                                            
                                      </button>
                                    </form>
                                    <form action="{{url('/edit_national',$national->national_id)}}" method="GET">
                                      <button type="submit" class="btn btn-info pull-right">
                                        {!! csrf_field() !!}
                                        <i class="icon-edit icon-white"></i>  
                                        Edit                                            
                                      </button>
                                    </form>
                                </td>
                              </tr>
                              <?php 
                                }
                              ?>

                           </tbody>
                        </table>

                        </div>
                      </div>  
                    </div>
                  </div>

              </div>
               
          </div>
      
    </div>

 @include('footer') {{-- Include footer file --}} 
@endsection