<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\report;
use Auth;
use Carbon\Carbon;
class ReportController extends Controller 
{
    public function monthlyReport(Request $request) 
    {
    
    	$cond = '';
    	if($request->licensetype){
    		$cond .= ' AND tbl_license.type_id ='.$request->licensetype;
    	}
    	if($request->province){
    		$cond .= ' AND tbl_license.province_id ='.$request->province;
    	}
    	if($request->start_date && $request->end_date){
    		$cond .= " AND date(tbl_license.created_at) BETWEEN '".$request->start_date."'"." AND "."'".$request->end_date."'";
    	}
    	
      	$data = DB::SELECT(DB::raw('
      			SELECT
      				tbl_province.province,
      				tbl_province.province_id,
				 approved_by,
				 type_request,
				 sum(room) AS total_room,
				 COUNT(*) AS total,
				 typeofrequest
				FROM
				 tbl_license 
				 join tbl_province on tbl_license.province_id = tbl_province.province_id
				 join tbl_typeofrequest on tbl_typeofrequest.request_id = tbl_license.type_request
				WHERE tbl_license.license_id is not null
				 '.$cond.'
				GROUP BY
				tbl_province.province,
				 type_request,
				 approved_by,
				 tbl_province.province_id,
				 typeofrequest
				ORDER BY tbl_province.province_id, approved_by
      		'));
      	if($request->excel){
      		$this->monthlyReportExcel($data);
      	}
      	
      $typeOfLicense = DB::table('tbl_typeoflicense')
      					->get(['type_id', 'type']);

      	$province = DB::table('tbl_province')
      					->get(['province_id', 'province']);
      return view('layouts.pages.list_report', ['data' => $data, 'licenseType'=> $typeOfLicense, 'provinces'=>$province]);
    }

    public function monthlyReportExcel($data)
    {
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
            ->setLastModifiedBy("Maarten Balliauw")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");

        // Create a first sheet
        $styleHeader = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => ' #696969'),
                'size' => 18,
                'name' => 'Verdana'
            ));
        $styleArray = array(
            'font' => array(
                'bold' => true,
                'color' => array('rgb' => ' #696969'),
                'size' => 10,
                'name' => 'Verdana'
            ));
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:Z1');
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'Monthly Report')->getStyle('A1')->applyFromArray($styleHeader);
        $objPHPExcel->getActiveSheet()->setCellValue('A2', "No")->getStyle('A2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('B2', "Location")->getStyle('B2')->applyFromArray($styleArray);
        // MOT
        $objPHPExcel->getActiveSheet()->setCellValue('C2', "Number")->getStyle('C2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('D2', "Rooms")->getStyle('D2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('E2', "Number")->getStyle('E2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('F2', "Rooms")->getStyle('F2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('G2', "Number")->getStyle('G2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('H2', "Rooms")->getStyle('H2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('I2', "Number")->getStyle('I2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('G2', "Rooms")->getStyle('G2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('K2', "Number")->getStyle('K2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('L2', "Rooms")->getStyle('L2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('M2', "Number")->getStyle('M2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('N2', "Rooms")->getStyle('N2')->applyFromArray($styleArray);
        //-----------------------------------------
        // DOT
        $objPHPExcel->getActiveSheet()->setCellValue('O2', "Number")->getStyle('O2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('P2', "Rooms")->getStyle('P2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('Q2', "Number")->getStyle('Q2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('R2', "Rooms")->getStyle('R2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('S2', "Number")->getStyle('S2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('T2', "Rooms")->getStyle('T2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('U2', "Number")->getStyle('U2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('V2', "Rooms")->getStyle('V2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('W2', "Number")->getStyle('W2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('X2', "Rooms")->getStyle('X2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('Y2', "Number")->getStyle('Y2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('Z2', "Rooms")->getStyle('Z2')->applyFromArray($styleArray);
        //-----------------------------------------
        // OWSO
        $objPHPExcel->getActiveSheet()->setCellValue('AA2', "Number")->getStyle('AA2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('AB2', "Rooms")->getStyle('AB2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('AC2', "Number")->getStyle('AC2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('AD2', "Rooms")->getStyle('AD2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('AE2', "Number")->getStyle('AE2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('AF2', "Rooms")->getStyle('AF2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('AG2', "Number")->getStyle('AG2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('AH2', "Rooms")->getStyle('AH2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('AI2', "Number")->getStyle('AI2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('AJ2', "Rooms")->getStyle('AJ2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('AK2', "Number")->getStyle('AK2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('AL2', "Rooms")->getStyle('AL2')->applyFromArray($styleArray);

        $objPHPExcel->getActiveSheet()->setCellValue('AM2', "Number")->getStyle('AM2')->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->setCellValue('AN2', "Rooms")->getStyle('AN2')->applyFromArray($styleArray);
        //-----------------------------------------

        $newDataArr = array();
          foreach ($data as $key => $value) {
            // MOT block
            $newDataArr[$value->province]['MOT']['New']['T'] = 0;
            $newDataArr[$value->province]['MOT']['New']['R'] = 0;
            $newDataArr[$value->province]['MOT']['Renew']['T'] = 0;
            $newDataArr[$value->province]['MOT']['Renew']['R'] = 0;
            $newDataArr[$value->province]['MOT']['Suspended']['T'] = 0;
            $newDataArr[$value->province]['MOT']['Suspended']['R'] = 0;
            $newDataArr[$value->province]['MOT']['Fine']['T'] = 0;
            $newDataArr[$value->province]['MOT']['Fine']['R'] = 0;
            $newDataArr[$value->province]['MOT']['Closed']['T'] = 0;
            $newDataArr[$value->province]['MOT']['Closed']['R'] = 0;
            $newDataArr[$value->province]['MOT']['Nolicense']['T'] = 0;
            $newDataArr[$value->province]['MOT']['Nolicense']['R'] = 0;
            // End of MOT block
            // DOT block
            $newDataArr[$value->province]['DOT']['New']['T'] = 0;
            $newDataArr[$value->province]['DOT']['New']['R'] = 0;
            $newDataArr[$value->province]['DOT']['Renew']['T'] = 0;
            $newDataArr[$value->province]['DOT']['Renew']['R'] = 0;
            $newDataArr[$value->province]['DOT']['Suspended']['T'] = 0;
            $newDataArr[$value->province]['DOT']['Suspended']['R'] = 0;
            $newDataArr[$value->province]['DOT']['Fine']['T'] = 0;
            $newDataArr[$value->province]['DOT']['Fine']['R'] = 0;
            $newDataArr[$value->province]['DOT']['Closed']['T'] = 0;
            $newDataArr[$value->province]['DOT']['Closed']['R'] = 0;
            $newDataArr[$value->province]['DOT']['Nolicense']['T'] = 0;
            $newDataArr[$value->province]['DOT']['Nolicense']['R'] = 0;
            // End of DOT block
            // OWSO block
            $newDataArr[$value->province]['OWSO']['New']['T'] = 0;
            $newDataArr[$value->province]['OWSO']['New']['R'] = 0;
            $newDataArr[$value->province]['OWSO']['Renew']['T'] = 0;
            $newDataArr[$value->province]['OWSO']['Renew']['R'] = 0;
            $newDataArr[$value->province]['OWSO']['Suspended']['T'] = 0;
            $newDataArr[$value->province]['OWSO']['Suspended']['R'] = 0;
            $newDataArr[$value->province]['OWSO']['Fine']['T'] = 0;
            $newDataArr[$value->province]['OWSO']['Fine']['R'] = 0;
            $newDataArr[$value->province]['OWSO']['Closed']['T'] = 0;
            $newDataArr[$value->province]['OWSO']['Closed']['R'] = 0;
            $newDataArr[$value->province]['OWSO']['Nolicense']['T'] = 0;
            $newDataArr[$value->province]['OWSO']['Nolicense']['R'] = 0;
            // End of OWSO block
          }
          foreach ($data as $key => $value) {

            $t = 0;
            $r = 0;
            if($value->total != null)
              $t = $value->total;
            if($value->total_room != null)
              $r = $value->total_room;
            $newDataArr[$value->province][$value->approved_by][$value->typeofrequest]['T'] = $t;
            $newDataArr[$value->province][$value->approved_by][$value->typeofrequest]['R'] = $r;
          }

        // Freeze panes
        $objPHPExcel->getActiveSheet()->freezePane('A3');

        // Rows to repeat at top
        $objPHPExcel->getActiveSheet()->getPageSetup()->setRowsToRepeatAtTopByStartAndEnd(1, 1);

        //Set border color of a cell
        $BStyle = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );

        $i = 1;
        // MOT
        $totalMNewT = 0;
        $totalMNewR = 0;
        $totalMRenewT = 0;
        $totalMRenewR = 0;
        $totalMSuspendedT = 0;
        $totalMSuspendedR = 0;
        $totalMFineT = 0;
        $totalMFineR = 0;
        $totalMClosedT = 0;
        $totalMClosedR = 0;
        $totalMNolicenseT = 0;
        $totalMNolicenseR = 0;
        // DOT
        $totalDNewT = 0;
        $totalDNewR = 0;
        $totalDRenewT = 0;
        $totalDRenewR = 0;
        $totalDSuspendedT = 0;
        $totalDSuspendedR = 0;
        $totalDFineT = 0;
        $totalDFineR = 0;
        $totalDClosedT = 0;
        $totalDClosedR = 0;
        $totalDNolicenseT = 0;
        $totalDNolicenseR = 0;
        // OWSO
        $totalONewT = 0;
        $totalONewR = 0;
        $totalORenewT = 0;
        $totalORenewR = 0;
        $totalOSuspendedT = 0;
        $totalOSuspendedR = 0;
        $totalOFineT = 0;
        $totalOFineR = 0;
        $totalOClosedT = 0;
        $totalOClosedR = 0;
        $totalONolicenseT = 0;
        $totalONolicenseR = 0;

        $allTotalT = 0;
        $allTotalR = 0;
        $i = 3;
        $n = 1;
        foreach ($newDataArr as $keys => $values) {
            $total = 0;
            $totalRoom = 0;
            $objPHPExcel->getActiveSheet()->setCellValue('A' . $i, $n);
            $objPHPExcel->getActiveSheet()->setCellValueExplicit('B' . $i, $keys);
            // Start
            foreach ($values as $key => $value) {
                if($key == 'MOT'){
                  foreach ($value as $k => $val) {
                    if($val['T'] > 0)
                      $total = $total+$val['T'];
                    if($val['R'] > 0)
                      $totalRoom = $totalRoom+$val['R'];

                    if($k == 'New'){
                      $objPHPExcel->getActiveSheet()->setCellValue('C' . $i, $val['T']);
                      $objPHPExcel->getActiveSheet()->setCellValue('D' . $i, $val['R']);
                      $totalMNewT = $totalMNewT+$val['T'];
                      $totalMNewR = $totalMNewR+$val['R'];
                    }elseif ($k == 'Renew') {
                      $objPHPExcel->getActiveSheet()->setCellValue('E' . $i, $val['T']);
                      $objPHPExcel->getActiveSheet()->setCellValue('F' . $i, $val['R']);
                      $totalMRenewT = $totalMRenewT+$val['T'];
                      $totalMRenewR = $totalMRenewR+$val['R'];
                    }elseif ($k == 'Suspended') {
                      $objPHPExcel->getActiveSheet()->setCellValue('G' . $i, $val['T']);
                      $objPHPExcel->getActiveSheet()->setCellValue('H' . $i, $val['R']);
                      $totalMSuspendedT = $totalMSuspendedT+$val['T'];
                      $totalMSuspendedR = $totalMSuspendedR+$val['R'];
                    }elseif ($k == 'Fine') {
                      $objPHPExcel->getActiveSheet()->setCellValue('I' . $i, $val['T']);
                      $objPHPExcel->getActiveSheet()->setCellValue('J' . $i, $val['R']);
                      $totalMFineT = $totalMFineT+$val['T'];
                      $totalMFineR = $totalMFineR+$val['R'];
                    }elseif ($k == 'Closed') {
                      $objPHPExcel->getActiveSheet()->setCellValue('K' . $i, $val['T']);
                      $objPHPExcel->getActiveSheet()->setCellValue('L' . $i, $val['R']);
                      $totalMClosedT = $totalMClosedT+$val['T'];
                      $totalMClosedR = $totalMClosedR+$val['R'];
                    }elseif ($k == 'Nolicense') {
                      $objPHPExcel->getActiveSheet()->setCellValue('M' . $i, $val['T']);
                      $objPHPExcel->getActiveSheet()->setCellValue('N' . $i, $val['R']);
                      $totalMNolicenseT = $totalMNolicenseT+$val['T'];
                      $totalMNolicenseR = $totalMNolicenseR+$val['R'];
                    }
                  }
                }elseif($key == 'DOT'){
                  foreach ($value as $k => $val) {
                    if($val['T'] > 0)
                      $total = $total+$val['T'];
                    if($val['R'] > 0)
                      $totalRoom = $totalRoom+$val['R'];

                    if($k == 'New'){
                      $objPHPExcel->getActiveSheet()->setCellValue('O' . $i, $val['T']);
                      $objPHPExcel->getActiveSheet()->setCellValue('P' . $i, $val['R']);
                      $totalDNewT = $totalDNewT+$val['T'];
                      $totalDNewR = $totalDNewR+$val['R'];
                    }elseif ($k == 'Renew') {
                      $objPHPExcel->getActiveSheet()->setCellValue('Q' . $i, $val['T']);
                      $objPHPExcel->getActiveSheet()->setCellValue('R' . $i, $val['R']);
                      $totalDRenewT = $totalDRenewT+$val['T'];
                      $totaldRenewR = $totalDRenewR+$val['R'];
                    }elseif ($k == 'Suspended') {
                      $objPHPExcel->getActiveSheet()->setCellValue('S' . $i, $val['T']);
                      $objPHPExcel->getActiveSheet()->setCellValue('T' . $i, $val['R']);
                      $totalDSuspendedT = $totalDSuspendedT+$val['T'];
                      $totalDSuspendedR = $totalDSuspendedR+$val['R'];
                    }elseif ($k == 'Fine') {
                      $objPHPExcel->getActiveSheet()->setCellValue('U' . $i, $val['T']);
                      $objPHPExcel->getActiveSheet()->setCellValue('V' . $i, $val['R']);
                      $totalDFineT = $totalDFineT+$val['T'];
                      $totalDFineR = $totalDFineR+$val['R'];
                    }elseif ($k == 'Closed') {
                      $objPHPExcel->getActiveSheet()->setCellValue('W' . $i, $val['T']);
                      $objPHPExcel->getActiveSheet()->setCellValue('X' . $i, $val['R']);
                      $totalDClosedT = $totalDClosedT+$val['T'];
                      $totalDClosedR = $totalDClosedR+$val['R'];
                    }elseif ($k == 'Nolicense') {
                      $objPHPExcel->getActiveSheet()->setCellValue('Y' . $i, $val['T']);
                      $objPHPExcel->getActiveSheet()->setCellValue('Z' . $i, $val['R']);
                      $totalDNolicenseT = $totalDNolicenseT+$val['T'];
                      $totalDNolicenseR = $totalDNolicenseR+$val['R'];
                    }
                  }
                }elseif($key == 'OWSO'){
                  foreach ($value as $k => $val) {
                    if($val['T'] > 0)
                      $total = $total+$val['T'];
                    if($val['R'] > 0)
                      $totalRoom = $totalRoom+$val['R'];

                    if($k == 'New'){
                      $objPHPExcel->getActiveSheet()->setCellValue('AA' . $i, $val['T']);
                      $objPHPExcel->getActiveSheet()->setCellValue('AB' . $i, $val['R']);
                      $totalONewT = $totalONewT+$val['T'];
                      $totalONewR = $totalONewR+$val['R'];
                    }elseif ($k == 'Renew') {
                      $objPHPExcel->getActiveSheet()->setCellValue('AC' . $i, $val['T']);
                      $objPHPExcel->getActiveSheet()->setCellValue('AD' . $i, $val['R']);
                      $totalORenewT = $totalORenewT+$val['T'];
                      $totalORenewR = $totalORenewR+$val['R'];
                    }elseif ($k == 'Suspended') {
                      $objPHPExcel->getActiveSheet()->setCellValue('AE' . $i, $val['T']);
                      $objPHPExcel->getActiveSheet()->setCellValue('AF' . $i, $val['R']);
                      $totalOSuspendedT = $totalOSuspendedT+$val['T'];
                      $totalOSuspendedR = $totalOSuspendedR+$val['R'];
                    }elseif ($k == 'Fine') {
                      $objPHPExcel->getActiveSheet()->setCellValue('AG' . $i, $val['T']);
                      $objPHPExcel->getActiveSheet()->setCellValue('AH' . $i, $val['R']);
                      $totalOFineT = $totalOFineT+$val['T'];
                      $totalOFineR = $totalOFineR+$val['R'];
                    }elseif ($k == 'Closed') {
                      $objPHPExcel->getActiveSheet()->setCellValue('AI' . $i, $val['T']);
                      $objPHPExcel->getActiveSheet()->setCellValue('AJ' . $i, $val['R']);
                      $totalOClosedT = $totalOClosedT+$val['T'];
                      $totalOClosedR = $totalOClosedR+$val['R'];
                    }elseif ($k == 'Nolicense') {
                      $objPHPExcel->getActiveSheet()->setCellValue('AK' . $i, $val['T']);
                      $objPHPExcel->getActiveSheet()->setCellValue('AL' . $i, $val['R']);
                      $totalONolicenseT = $totalONolicenseT+$val['T'];
                      $totalONolicenseR = $totalONolicenseR+$val['R'];
                    }
                  }
                }
              }
            // End 
            $objPHPExcel->getActiveSheet()->setCellValue('AM' . $i, $total);
            $objPHPExcel->getActiveSheet()->setCellValue('AN' . $i, $totalRoom);
            $allTotalT = $allTotalT+$total;
            $allTotalR = $allTotalR+$totalRoom;
            //Set border color of a cell
            $objPHPExcel->getActiveSheet()->getStyle('A' . ($i-1) . ':AN' . $i)->applyFromArray($BStyle);
            $i++; 
            $n++;
        }
       
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        ExcelHelper::setNameAndSaveFile('Monthly Report' . date('H-i-s'), $objPHPExcel);
        exit;
    }

    public function annualReport(Request $request)
    {
      $cond = '';
      if($request->licensetype){
        $cond .= ' AND tbl_license.type_id ='.$request->licensetype;
      }
      if($request->start_date && $request->end_date){
        $cond .= " AND date(tbl_license.created_at) BETWEEN '".$request->start_date."'"." AND "."'".$request->end_date."'";
      }
      
        $data = DB::SELECT(DB::raw('
            SELECT
              tbl_province.province,
              tbl_province.province_id,
         approved_by,
         type_request,
         sum(room) AS total_room,
         COUNT(*) AS total,
         typeofrequest
        FROM
         tbl_license 
         join tbl_province on tbl_license.province_id = tbl_province.province_id
         join tbl_typeofrequest on tbl_typeofrequest.request_id = tbl_license.type_request
        WHERE tbl_license.license_id is not null
         '.$cond.'
        GROUP BY
        tbl_province.province,
         type_request,
         approved_by,
         tbl_province.province_id,
         typeofrequest
        ORDER BY tbl_province.province_id, approved_by
          '));
        if($request->excel){
          $this->monthlyReportExcel($data);
        }
        $licensetype = \DB::table('tbl_typeoflicense')->get();
        return view('layouts.pages.annual_report',['licensetypes'=> $licensetype, 'data' => $data]);
      }





}
