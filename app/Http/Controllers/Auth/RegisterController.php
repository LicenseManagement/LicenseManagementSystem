<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest');
    // }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */

    public function store(Request $request){

        $user = new User;
        $user->name = $request->name;
        $user->password = bcrypt($request->password);
        $user->email = $request->email;
        $user->position_id =$request->position;
        $user->gender=$request->gender;
        $user->role_id=$request->role;
        $user->province_id=$request->province;
        $user->status=$request->status;

        $user->save();
        

        return redirect('/view_user');
        }
    public function index()
    
    {
        $gender = \DB::table('users')->get();
        $status = \DB::table('users')->get();
        $province = \DB::table('tbl_province')->get();
        $role = \DB::table('tbl_role')->get();
        $position = \DB::table('tbl_position')->get();
        return view('auth.register',['genders' => $gender,
                                     'status_name' => $status,
                                     'province_name'=> $province,
                                     'role_name'=> $role,
                                     'position_name'=> $position]);
    }
}
