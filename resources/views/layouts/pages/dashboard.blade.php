@extends('layouts.app')
@section('content')
@include('header') {{-- Include header file --}}

<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Dashboard</div>
            <div class="panel-body">
             
                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered bootstrap-datatable" id="datatable" style="margin-left: 0px; margin-top:10px;">
                <thead>
                  <tr>
                    <th style="color:#000;"><strong>Type of License</strong></th>
                    <th style="color:#000;">Number</th>
                    <th style="color:#000;">Rooms</th>
                    <th style="color:#000;">Percentage</th>
                  </tr>
                </thead>   
                <tbody>
                  <tr>
                    
                    <td>Hotel</td>
                    <td>647</td>
                    <td>36734</td>
                    <td>
                      <div class="progress thin">
                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="73" aria-valuemin="0" aria-valuemax="100" style="width: 73%">
                        </div>
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="27" aria-valuemin="0" aria-valuemax="100" style="width: 27%">
                          </div>
                      </div>
                      <span class="sr-only">73%</span>    
                    </td>
                  </tr>
                  <tr>
                   
                    <td>Apartment</td>
                    <td>420</td>
                    <td>2924</td>
                    <td>
                      <div class="progress thin">
                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="57" aria-valuemin="0" aria-valuemax="100" style="width: 57%">
                        </div>
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="43" aria-valuemin="0" aria-valuemax="100" style="width: 43%">
                        </div>
                      </div>
                      <span class="sr-only">57%</span>    
                    </td>
                  </tr>
                  <tr>
                    
                    <td>Guest House</td>
                    <td>2047</td>
                    <td>3008</td>
                    <td>
                      <div class="progress thin">
                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="93" aria-valuemin="0" aria-valuemax="100" style="width: 93%">
                        </div>
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="7" aria-valuemin="0" aria-valuemax="100" style="width: 7%">
                          </div>
                      </div>
                      <span class="sr-only">93%</span>    
                    </td>
                  </tr>
                  <tr>
                   
                    <td>Restaurant</td>
                    <td>210</td>
                    <td>1345</td>
                    <td>
                      <div class="progress thin">
                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                          </div>
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                          </div>
                      </div>
                      <span class="sr-only">20%</span>    
                    </td>
                  </tr>
                  <tr>
                   
                    <td>Canteen</td>
                    <td>210</td>
                    <td>3621</td>
                    <td>
                      <div class="progress thin">
                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                          </div>
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                          </div>
                      </div>
                      <span class="sr-only">20%</span>    
                    </td>
                  </tr>
                  <tr>
                   
                    <td>Massage</td>
                    <td>210</td>
                    <td>102</td>
                    <td>
                      <div class="progress thin">
                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                          </div>
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                          </div>
                      </div>
                      <span class="sr-only">20%</span>    
                    </td>
                  </tr>
                  <tr>
                  
                    <td>Sport</td>
                    <td>210</td>
                    <td>102</td>
                    <td>
                      <div class="progress thin">
                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                          </div>
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                          </div>
                      </div>
                      <span class="sr-only">20%</span>    
                    </td>
                  </tr>
                  <tr>
                   
                    <td>Karaoke</td>
                    <td>210</td>
                    <td>102</td>
                    <td>
                      <div class="progress thin">
                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                          </div>
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                          </div>
                      </div>
                      <span class="sr-only">20%</span>    
                    </td>
                  </tr>
                </tbody>
              </table>
        
            </div>
          </div>
        </div>
    </div>
 

@include('footer') {{-- Include footer file --}} 
@endsection

