<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Province;

class ProvinceController extends Controller
{
     public function index()
    {
        $province = DB::table('tbl_province')->paginate(10);
    	  return view('layouts.pages.province', ['tbl_province' => $province]);

    }
     public function store(Request $request)
    {
          $province = new Province;
          $province->province= $request->province;
          $province->description = $request->descrip;
          $province->save();

          return redirect()->back();
    }

     public function show()
    {
           
          $province = DB::table('tbl_province')->get();
    	   return view('layouts.pages.province', ['tbl_province' => $province]);
    }

}
