<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Viewlicense;
use DateTime;

class ViewlicenseController extends Controller
{
    //
     public function index()
    {
        return view('layouts.pages.viewlicense');
    }
    public function expiredlicense()
    { 
           $now = new DateTime(); 
           $license = DB::table('tbl_license')
                    ->join('tbl_national', 'tbl_national.national_id', '=', 'tbl_license.national_id')
                    ->join('tbl_typeoflicense', 'tbl_typeoflicense.type_id', '=', 'tbl_license.type_id')
                    ->join('tbl_province', 'tbl_province.province_id', '=', 'tbl_license.province_id')
                    ->join('users','id', '=', 'user_createid')
                    ->where ('tbl_license.expired_date','<',$now)->paginate(10);
         return view('layouts.pages.expired_license', ['tbl_license' => $license]);
    }
    public function EditLicenseExpired($license_id){

        $license = DB::table('tbl_license')
            ->join('tbl_national', 'tbl_national.national_id', '=', 'tbl_license.national_id')
            ->join('tbl_typeoflicense', 'tbl_typeoflicense.type_id', '=', 'tbl_license.type_id')
            ->join('tbl_province', 'tbl_province.province_id', '=', 'tbl_license.province_id')
            ->join('users','id', '=', 'user_createid')
            ->where('license_id', $license_id)
            ->first();

        $typeoflicense = DB::table('tbl_typeoflicense')->get();
        $national = DB::table('tbl_national')->get();
        $typeofrequest = DB::table('tbl_typeofrequest')->get();
        $province = DB::table('tbl_province')->get();
        return view('layouts.pages.edit_expired_license', ['licenses' => $license,
                                                            'nationals' => $national,
                                                            'typeoflicenses'=>$typeoflicense, 
                                                            'typeofrequests' => $typeofrequest, 
                                                            'provinces' => $province]);
       
    }
    public function updateExpiredLicense(Request $request , $license_id){
         $input = $request->all();
        $license_id = $input['license_id'];
        unset($input['_token']);
        $license = DB::table('tbl_license')->where('license_id',$license_id)->update($input);       
        return redirect::to('/expiredlicense_license');
    }

    public function view_detail($license_id){

        $license = DB::table('tbl_license')
            ->join('tbl_national', 'tbl_national.national_id', '=', 'tbl_license.national_id')
            ->join('tbl_typeoflicense', 'tbl_typeoflicense.type_id', '=', 'tbl_license.type_id')
            ->join('tbl_province', 'tbl_province.province_id', '=', 'tbl_license.province_id')
            ->join('users','id', '=', 'user_createid')
            ->where('license_id',$license_id)
            ->first();

        $typeoflicense = DB::table('tbl_typeoflicense')->get();
        $national = DB::table('tbl_national')->get();
        $typeofrequest = DB::table('tbl_typeofrequest')->get();
        $province = DB::table('tbl_province')->get();
        return view('layouts.pages.view_license_detail', ['licenses' => $license,
                                                            'nationals' => $national,
                                                            'typeoflicenses'=>$typeoflicense, 
                                                            'typeofrequests' => $typeofrequest, 
                                                            'provinces' => $province]); 
    }

     public function edit_license($license_id){

        $license = DB::table('tbl_license')
            ->join('tbl_national', 'tbl_national.national_id', '=', 'tbl_license.national_id')
            ->join('tbl_typeoflicense', 'tbl_typeoflicense.type_id', '=', 'tbl_license.type_id')
            ->join('tbl_province', 'tbl_province.province_id', '=', 'tbl_license.province_id')
            ->join('users','id', '=', 'user_createid')
            ->where('license_id', $license_id)
            ->first();

        $typeoflicense = DB::table('tbl_typeoflicense')->get();
        $national = DB::table('tbl_national')->get();
        $typeofrequest = DB::table('tbl_typeofrequest')->get();
        $province = DB::table('tbl_province')->get();
        return view('layouts.pages.edit_expired_license', ['licenses' => $license,
                                                            'nationals' => $national,
                                                            'typeoflicenses'=>$typeoflicense, 
                                                            'typeofrequests' => $typeofrequest, 
                                                            'provinces' => $province]);
       
    }
}
