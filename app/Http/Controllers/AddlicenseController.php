<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\addlicense;
use Auth;
use DateTime;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class AddlicenseController extends Controller
{
    public function index() {
      $national = DB::table('tbl_national')->get();
      $type = DB::table('tbl_typeoflicense')->get();
      $province = DB::table('tbl_province')->get(); 
        return view('layouts.pages.addlicense', ['national_name' => $national,'typename' => $type,'province_name' => $province]);
    }

    public function store(Request $request) {
	    	// dd( $request);
          $license = new addlicense;
          $license->ownername = $request->owner_name;
          $license->manager = $request->manager_name;
          $license->sex = $request->sex;
          $license->national_id = $request->nationality_license;
          $license->type_request = $request->type_request;
          $license->type_id = $request->type_license;
          $license->approved_by = $request->approve;
          $license->name_license = $request->name_license;
          $license->address = $request->address;
          $license->province_id = $request->n_provinces;
          $license->room = $request->rooms;
          $license->chair = $request->chairs;
          $license->table = $request->table;
          $license->start_date = $request->start_date;
          $license->expired_date = $request->expire_date;
          $license->establishe_year = $request->est_year;
          $license->telephone = $request->phone;
          $license->total_staff = $request->total_staffs;
          $license->women_staff = $request->w_staffs;
          $license->foreign_staff = $request->for_staffs;
          $license->document = $request->file_input;
          $license->user_createid = Auth::user()->id;
          $license->save();

          return redirect::to('/viewlicense');
    }
    
    public function show()
      {

          if(Auth::user()->id == 1){
              $cond = array(1, 2);
          }else {
            $cond = array(Auth::user()->id);
          }

          $now = new DateTime();
          $license = DB::table('tbl_license')
            ->join('tbl_national', 'tbl_national.national_id', '=', 'tbl_license.national_id')
            ->join('tbl_typeoflicense', 'tbl_typeoflicense.type_id', '=', 'tbl_license.type_id')
            ->join('tbl_province', 'tbl_province.province_id', '=', 'tbl_license.province_id')
            ->join('users','id', '=', 'user_createid')
            ->whereIn('users.id', $cond)
            ->where ('tbl_license.expired_date','>',$now)->paginate(10);

           return view('layouts.pages.viewlicense', ['licenses' => $license]);

      }

}
