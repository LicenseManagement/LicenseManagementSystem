@extends('layouts.app') 
@section('content')
@include('header') {{-- Include header file --}} 
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <!--<form role="form" method="POST" action="{{ route('register.new_user') }}">-->
                    <form class="col-xs-12" role="form" method="POST" action="{{ route('register.new_user') }}">
                        {{ csrf_field() }}
                        <div class="col-sm-6">
                            <div class="form-group row{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">Name *</label>
                            <div class="col-sm-8">
                            <input id="name" type="text" class="form-control" placeholder="Full Name" name="name" value="{{ old('name') }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>    
                        </div>

                        <div class="form-group row{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="inputEmail3" class="col-sm-3 col-form-label">E-Mail Address</label>
                            <div class="col-sm-8">
                            <input id="email" type="email" class="form-control" id="inputEmail3" placeholder="Email" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                         <div class="form-group row">
                            <label for="Sex" class="col-sm-3 col-form-label">Gender</label>
                            <div class="col-sm-8">
                                <select class="form-control" id="gender" name="gender">
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>
                             </div>
                             </div>
                        <div class="form-group row{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-sm-3 col-form-label">Password *</label>
                            <div class="col-sm-8">
                            <input type="password" class="form-control" id="inputPassword3" placeholder="Password" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>         

                        </div>
                        <div class="col-sm-6">
                        <div class="form-group row">
                            <label for="role" class="col-sm-3 col-form-label">Role</label>
                            <div class="col-sm-8">
                             <select class="form-control" id="role" name="role">
                                @foreach($role_name as $role)
                                  <option value="{{$role->roleid}}">{{$role->role_name}}</option>
                                  @endforeach
                            </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Position" class="col-sm-3 col-form-label">Position</label>
                            <div class="col-sm-8">
                            <select class="form-control" id="position" name="position">
                                @foreach($position_name as $position)
                                  <option value="{{$position->positionid}}">{{$position->position_name}}</option>
                                  @endforeach
                            </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="status" class="col-sm-3 col-form-label">Status</label>
                            <div class="col-sm-8">
                            <select class="form-control" id="status" name="status">      
                                  <option value="1">Active</option>
                                  <option value="0">Inactive</option>
                            </select>
                            </div>
                        </div> 
                        <div class="form-group row">
                            <label for="province" class="col-sm-3 col-form-label">Province</label>
                            <div class="col-sm-8">
                          <select class="form-control" id="province" name="province">
                                @foreach($province_name as $province)
                                  <option value="{{$province->province_id}}">{{$province->province}}</option>
                                @endforeach
                            </select>
                            </div>
                        </div>                     
                        <div class="form-group row">
                            <div class="col-md-offset-9">
                                <button type="submit" class="btn btn-primary" name="submit">
                                    Register
                                </button>
                            </div>
                        </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>
 @include('footer') {{-- Include footer file --}} 
@endsection
