@extends('layouts.app')
@section('content')
@include('header') {{-- Include header file --}} 
<div class="container">

  <div class="row">

    <div class="panel panel-default">
      <div class="panel-heading">View License Detail</div>
      <div class="panel-body">
      <div class="col-md-6">
          <table cellpadding="0" cellspacing="0" border="0" class="table table-striped" id="datatable">
                        <tdead>
                              <tr>
                               <td><label for="Sex" class="col-sm-12 col-form-label">Owner Name</label></td>
                                <td>:</td>
                                <td><?php echo $licenses->ownername;?></td>
                                
                              </tr>
                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">Sex</label></td> 
                                <td>:</td>                                  
                                <td><?php echo $licenses->sex;?></td>
                              </tr>
                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">Nationality </label></td> 
                                <td>:</td>                                  
                                <td><?php echo $licenses->national;?></td>
                              </tr>
                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">Type Of Request</label></td>  
                                <td>:</td>                                 
                                <td><?php echo $licenses->type_request;?></td>
                              </tr>
                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">Type Of License</label></td> 
                                <td>:</td>                                  
                                <td><?php echo $licenses->type;?></td>
                              </tr>
                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">Approved By </label> </td>  
                                <td>:</td>                                 
                                <td><?php echo $licenses->approved_by;?></td>
                              </tr>
                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">Name Of License </label></t>  
                                <td>:</td>                                 
                                <td><?php echo $licenses->name_license;?></td>
                              </tr>
                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">Address </label></td>      
                                <td>:</td>                             
                                <td><?php echo $licenses->address;?></td>
                              </tr>
                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">Province</label> </td>  
                                <td>:</td>                                 
                                <td><?php echo $licenses->province;?></td>
                              </tr>

                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">Rooms </label></td>
                                <td>:</td>                                   
                                <td><?php echo $licenses->room;?></td>
                              </tr>
                              
                    
                        </tdead>                     
          </table>         
      </div>
      <div class="col-md-6">
          <table cellpadding="0" cellspacing="0" border="0" class="table table-striped" id="datatable">
                        <tdead>
                              
                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">chairs </label></td>   
                                <td>:</td>                                
                                <td><?php echo $licenses->chair;?></td>
                              </tr>
                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">Tables </label></td>    
                                <td>:</td>                               
                                <td><?php echo $licenses->table;?></td>
                              </tr>
                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">Started Date </label></td> 
                                <td>:</td>                                  
                                <td><?php echo $licenses->start_date;?></td>
                              </tr>
                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">Expired Date </label></td>  
                                <td>:</td>                                 
                                <td><?php echo $licenses->expired_date;?></td>
                              </tr>


                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">Established Years </label></td>  
                                <td>:</td>                                 
                                <td><?php echo $licenses->establishe_year;?></td>
                              </tr>
                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">Telephone </label></td> 
                                <td>:</td>                                  
                                <td><?php echo $licenses->telephone;?></td>
                              </tr>
                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">Total Staffs </label></td>  
                                <td>:</td>                                 
                                <td><?php echo $licenses->total_staff;?></td>
                              </tr>
                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">Women Staffs </label></td> 
                                <td>:</td>                                  
                                <td><?php echo $licenses->women_staff;?></td>
                              </tr>
                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">Foreign Staffs </label></td>
                                <td>:</td>                                   
                                <td><?php echo $licenses->foreign_staff;?></td>
                              </tr>
                              <tr>
                                <td><label for="Sex" class="col-sm-12 col-form-label">Document </label></td> 
                                <td>:</td>                                  
                                <td><?php echo $licenses->document;?></td>
                              </tr>
                    
                        </tdead>                     
          </table> 
           <a href="{{URL::to('viewlicense')}}"><button type="submit" class="btn btn-primary">Close</button></a>       
      </div>
      </div>
    </div>
  </div>
</div> 
@include('footer') {{-- Include footer file --}} 
@endsection
@section('script')
<script type="text/javascript">
  $('.startdate').datepicker({

    dateFormat: "yy-mm-dd"

    // dateFormat: "yy-mm-dd"
  });
  $('.expiredate').datepicker({
    dateFormat: "yy-mm-dd"
  });
</script>
@endsection