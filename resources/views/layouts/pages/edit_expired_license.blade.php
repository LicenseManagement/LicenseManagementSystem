@extends('layouts.app')
@section('content')
@include('header') {{-- Include header file --}} 
<div class="container">

	<div class="row">

		<div class="panel panel-default">
			<div class="panel-heading">Edit Expired License</div>
			<div class="panel-body">
				<form action="{{URL::to('/expired_license/update')}}" method="POST">
					{!! csrf_field() !!}

					<div class="col-xs-6">
						<div class="form-group">
							<label for="exampleInputEmail1">Owner Name</label>
							<input name="owner_name" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$licenses->ownername}}">				    
						</div>
						
						<div class="form-group">
							<label for="exampleSelect1">Sex</label>
							<select name="sex" class="form-control" id="exampleSelect1">
								<option>N/A</option>
								<option>Mr.</option>
								<option>Ms.</option>
								<option>Mrs.</option>
							</select>
						</div>
						<div class="form-group">
							<label for="exampleSelect1">Nationality</label>
							<select name="nationality_license" class="form-control" id="exampleSelect1">
								@foreach($nationals as $name)
								<option value="{{$name->national_id}}">{{$name->national}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label for="exampleSelect1">Type Of Request</label>
							<select name="type_request" class="form-control" id="exampleSelect1">
								<option>New</option>
								<option>Renew</option>
								<option>Closed</option>
								<option>Suspend</option>
								<option>No License</option>
							</select>
						</div>
						<div class="form-group">
							<label for="exampleSelect1">TypeOfLicense</label>
							<select name="type_license" class="form-control" id="exampleSelect1">
								@foreach($typeoflicenses as $type)
								<option value="{{$type->type_id}}">{{$type->type}}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group">
							<label for="exampleSelect1">Approved by</label>
							<select name="approve" class="form-control" id="exampleSelect1">
								<option>MOT</option>
								<option>DOT</option>
								<option>OWSO</option>
							</select>
						</div>						
						<div class="form-group">
							<label for="exampleTextarea">NameOfLicense</label>
							<input type="text" name="name_license" class="form-control" rows="3" value="{{$licenses->name_license}}">
						</div>
						<div class="form-group">
							<label for="exampleTextarea">Address</label>
							<textarea name="address" class="form-control" id="exampleTextarea" rows="3" value="{{$licenses->address}}"></textarea>
						</div>
						<div class="form-group">
							<label for="exampleSelect1">Provinces</label>
							<select name="n_provinces" class="form-control" id="exampleSelect1">
								@foreach($provinces as $province)
								<option value="{{$province->province_id}}">{{$province->province}}</option>
								@endforeach
							</select>
						</div>							  

					</div>

					<div class="col-xs-6">

						<div class="form-group">
							<label for="exampleInputEmail1">Rooms</label>
							<input name="rooms" type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$licenses->room}}" >
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Chairs</label>
							<input name="chairs" type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="chair">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Table</label>
							<input name="table" type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$licenses->table}}">				    
						</div>

						<div class="form-group">
							<label>Started Date</label>
							<input type="text" name="start_date" class="form-control startdate" value="{{$licenses->start_date}}" >
						</div>

						<div class="form-group">
							<label>Expired Date</label>
							<input type="text" name="expire_date" class="form-control expiredate" value="{{$licenses->expired_date}}">
						</div>

						<div class="form-group">
							<label>Established Year</label>
							<input name="est_year" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$licenses->establishe_year}}" >
						</div>

						<div class="form-group">
							<label for="exampleInputEmail1">Telephone</label>
							<input name="phone" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$licenses->telephone}}">
						</div>	
						<div class="form-group">
							<label for="exampleInputEmail1">Total Staffs</label>
							<input name="total_staffs" type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$licenses->total_staff}}">				    
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Women Staffs</label>
							<input name="w_staffs" type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$licenses->women_staff}}">
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Foreign Staffs</label>
							<input name="for_staffs" type="number" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$licenses->foreign_staff}}">
						</div>  							 


						<div class="form-group">
							<label for="exampleInputFile">File input</label>
							<input name="file_input" type="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
						</div>
						 <input type="hidden" name="id" value="{{$licenses->license_id}}"/>
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</form>

			</div>
		</div>
	</div>

</div> 
@include('footer') {{-- Include footer file --}} 
@endsection
@section('script')
<script type="text/javascript">
	$('.startdate').datepicker({

		dateFormat: "yy-mm-dd"

		// dateFormat: "yy-mm-dd"
	});
	$('.expiredate').datepicker({
		dateFormat: "yy-mm-dd"
	});
</script>
@endsection