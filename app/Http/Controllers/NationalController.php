<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\National;


class NationalController extends Controller
{
    //
     public function index()
    {
        $national = DB::table('tbl_national')->paginate(10);
        $national = DB::table('tbl_national')->where('is_deleted', 0)->get();
    	  return view('layouts.pages.national', ['tbl_national' => $national]);

        // return view('national');
    }
     public function store(Request $request)
    {
          $national = new National;
          $national->national = $request->nation;
          $national->description = $request->descrip;
          $national->save();

          return redirect()->back();
    }

     public function show()
    {
           
          $national = DB::table('tbl_national')->where('is_deleted', 0)->get();
    	   return view('layouts.pages.national', ['tbl_national' => $national]);
    }

    public function edit($national_id)
    {
      // dd($national_id);
          $national = DB::table('tbl_national')->where('national_id', $national_id)->get();
          // dd($national);
         return view('layouts.pages.edit_national', ['tbl_national' => $national]);
    }
    public function update(Request $request, $national_id)
    {
      // dd('above');
         $national = National::where('national_id', $national_id)->update([
            'national' => $request->nation,
            'description' => $request->descrip
          ]);
          return redirect()->route('national');
    }

    public function delete($national_id)
    {
      $national = National::where('national_id', $national_id)->update([
          'is_deleted' => 1
      ]);
      return redirect()->route('national');
    }

}
