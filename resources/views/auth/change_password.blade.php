@extends('layouts.app') 
@section('content')
@include('header') {{-- Include header file --}} 
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Change Password</div>
                <div class="panel-body">    
                @if (Session::has('success'))
                    <div class="alert alert-success">{!! Session::get('success') !!}</div>
                @endif
                @if (Session::has('failure'))

                    <div class="alert alert-danger">{!! Session::get('failure') !!}</div>
                @endif
                <form class="col-md-6 col-md-offset-3" role="form" method="POST" action="{{ url('change/password') }}">
                        {{ csrf_field() }}

                        <div class="form-group row{{ $errors->has('old') ? ' has-error' : '' }}"">
                            <label for="old" class="col-md-4 control-label">Old Password *</label>
                            <div class="col-md-6">
                            <input id="old" type="password" class="form-control" name="old" required>
                               @if ($errors->has('old'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('old') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">New Password</label>
                             <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>
                               @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                            </div>
                        </div>   
                        <div class="form-group row{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                            </div>
                        </div>                  
                        <div class="form-group row">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" name="submit">
                                    Change Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>
{{-- 
@if(session('success'))
<script type="text/javascript">
    $(document).ready(function(){
        alertify.error('{{session('error')}}');
    });
</script>
@enif --}}

 @include('footer') {{-- Include footer file --}} 

@endsection
