@extends('layouts.app')
@section('content')  
@include('header') {{-- Include header file --}} 

<link href="{{ asset('css/style.css') }}" rel="stylesheet">

 <div id="app">
    
        <div class="container" id="menu_file">
            <div class="row">
               
            </div>
        </div>
   

</div>
<div class="container">

    <div class="row">
        
            <div class="panel panel-default">
              <div class="panel-heading">List All Province</div>

                <div class="panel-body">
                   

                        <button type="button" class="btn btn-info" data-toggle="collapse" 
                            data-target="#demo">Add Province</button>

                            

                        <form action="{{route('province.stored')}}" method="POST">
                      {!! csrf_field() !!}

                            <div class="col-xs-6">
                                <div id="demo" class="collapse" class="form-group">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Province</label>
                                        <input type="text" name="province" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                                    </div>
                                  
                                    <div class="form-group">
                                        <label for="exampleTextarea">Description</label>
                                        <textarea class="form-control" id="exampleTextarea" name="descrip" rows="3"></textarea>
                                    </div>
                                 
                                        <button type="submit" class="btn btn-primary">Save</button>
                                        <button type="submit" class="btn btn-primary">Cancel</button>
                                </div>
                            </div>
                        </form>

                        <form class="navbar-form navbar-left">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="Search province....">
                              </div>
                              <button type="submit" class="btn btn-default">Go</button>
                        </form>

                     <div class="col-lg-12">

                      <div class="box-container-toggle">
                          <div class="box-content">

                            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered bootstrap-datatable" id="datatable">
                           <tbody>
                              <tr>
                                <th>Id</th>
                                <th>Province</th>                               
                                <th>Description</th>
                                <th>Action</th>
                              </tr>

                              <?php
                                  foreach ($tbl_province as $province) {
                                    
                               ?>
                                 <td class="center"><?php echo $province->province_id;?></td>
                                 <td class="center"><?php echo $province->province;?></td>
                                 <td class="center"><?php echo $province->description;?></td>
                                
                                 <td class="center">
                                    <a class="btn btn-info" href="edit" rel="tooltip" title="Edit"><i class="icon-edit icon-white"></i>  
                                        Edit </a>

                                    <a class="btn btn-danger" href="#" rel="tooltip" title="Delete">
                                         <i class="icon-trash icon-white"></i>  
                                        Delete                                            
                                     </a>
                                </td>
                              </tr>
                              <?php 
                                }
                              ?>
                           </tbody>
                        </table>
                        {{ $tbl_province->links() }} 
                      </div>
                      </div>  
                      
                  </div>


                </div>
               
            </div>
      
    </div>
   
</div>
 @include('footer') {{-- Include footer file --}} 
@endsection
