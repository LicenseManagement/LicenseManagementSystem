@extends('layouts.app')

@section('content')
@include('header') {{-- Include header file --}}  

<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<link href="{{ asset('css/style-responsive.css') }}" rel="stylesheet"> 
<link href="{{ asset('css/css/bootstrap.min.css') }}" rel="stylesheet"> 
<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet"> 
<link href="{{ asset('css/elegant-icons-style.css') }}" rel="stylesheet"> 
 <div id="app">
        <div class="container" id="menu_file">
            <div class="row">
               
            </div>
        </div>
</div>
<div class="container">

    <div class="row">
        
            <div class="panel panel-default">
              <div class="panel-heading">Update Nationality</div>

                <div class="panel-body">
                   

                   <!--      <button type="button" class="btn btn-info" data-toggle="collapse" 
                            data-target="#demo">Update National</button> -->

                    @foreach($tbl_national as $tbl_nation)
                        <form action="{{url('/update_national', $tbl_nation->national_id)}}" method="POST">
                      {!! csrf_field() !!}

                            <div class="col-xs-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">National</label>
                                         <input type="text" name="nation" class="form-control" id="exampleInputEmail1" value="{{$tbl_nation->national}}" aria-describedby="emailHelp" required="true">
                                        </div>
                                  
                                    <div class="form-group">
                                        <label for="exampleTextarea">Information</label>
                                        <textarea class="form-control" id="exampleTextarea" name="descrip" required="true"> {{$tbl_nation->description}}</textarea>
                                    </div>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                        <a href="{{URL::to('national')}}"><button type="submit" class="btn btn-primary">Close</button></a> 
                                </div>
                            </div>
                        </form>
                    @endforeach

                        
                      
                  </div>


                </div>
               
            </div>
 
   
</div>
 @include('footer') {{-- Include footer file --}} 
@endsection
