@extends('layouts.app') 
@section('content')
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="col-xs-6" role="form" method="POST" action="{{ route('register.new_user') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name">Name *</label>
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                       
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address *</label>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>

                         <div class="form-group">
                            <label for="Sex" class="col-md-4 control-label">Sex</label>
                                <select class="form-control" id="sex">
                                    @foreach($genders as $gender)
                                  <option value="">{{$gender->sex}}</option>
                                  @endforeach
                                </select>

                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password *</label>
                            <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>

                    </form>

                    <form class="col-xs-6" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="status">Status</label>
                            <select class="form-control" id="status" name="status">
                                @foreach($status_name as $status)
                                  <option>{{$status->status}}</option>
                                  @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="role" class="col-md-4 control-label">Role</label>
                             <select class="form-control" id="role" name="role">
                                @foreach($role_name as $role)
                                  <option>{{$role->role_name}}</option>
                                  @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="Position" class="col-md-4 control-label">Position</label>
                            <select class="form-control" id="position" name="position">
                                @foreach($position_name as $position)
                                  <option>{{$position->position_name}}</option>
                                  @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="province" class="col-md-4 control-label">Province</label>
                          <select class="form-control" id="province" name="province">
                                @foreach($province_name as $province)
                                  <option>{{$province->province}}</option>
                                  @endforeach
                            </select>
                        </div>                      
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary" name="submit">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>
@endsection
