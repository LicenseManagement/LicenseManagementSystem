<!DOCTYPE html>
 <html lang="en">
 <head>
   <meta charset="utf-8" />
   <title>Admin Simplenso - Meber List </title>
   <meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <meta name="description" content="HTML5 Admin Simplenso Template" />
   <meta name="author" content="ahoekie" />

@extends('layouts.app')

@section('content')
@include('header') {{-- Include header file --}}


<link href="{{ asset('css/elegant-icons-style.css') }}" rel="stylesheet"> 
<link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet"> 
<link href="{{ asset('css/style.css') }}" rel="stylesheet"> 
<link href="{{ asset('css/style-responsive.css') }}" rel="stylesheet"> 

 </head>  
 <body>
 <div id="app">
    
        <div class="container" id="menu_file">
            <div class="row">
                <div class="col-md-12 text-center">
                    
            ​​
                          
                    
                </div>
            </div>
        </div>
   

</div>
<div class="container">

    <div class="row">
        
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                   <div class="tab-pane" id="chartjs">
                      <div class="row">
                          <!-- Line -->
                          <div class="col-lg-6">
                              <section class="panel">
                                  <header class="panel-heading">
                                      Line
                                  </header>
                                  <div class="panel-body text-center">
                                      <canvas id="line" height="300" width="450"></canvas>
                                  </div>
                              </section>
                          </div>                      
                          <!-- Bar -->
                          <div class="col-lg-6">
                              <section class="panel">
                                  <header class="panel-heading">
                                      Bar
                                  </header>
                                  <div class="panel-body text-center">
                                      <canvas id="bar" height="300" width="500"></canvas>
                                  </div>
                              </section>
                          </div>
                      </div>
                      <div class="row">
                          <!-- Radar -->
                          <div class="col-lg-6">
                              <section class="panel">
                                  <header class="panel-heading">
                                      Radar
                                  </header>
                                  <div class="panel-body text-center">
                                      <canvas id="radar" height="300" width="400"></canvas>
                                  </div>
                              </section>
                          </div>
                          <!-- Polar Area -->
                          <div class="col-lg-6">
                              <section class="panel">
                                  <header class="panel-heading">
                                      Polar Area
                                  </header>
                                  <div class="panel-body text-center">
                                      <canvas id="polarArea" height="300" width="400"></canvas>
                                  </div>
                              </section>
                          </div>
                      </div>
                      <div class="row">
                          
                          <!-- Pie -->
                          <div class="col-lg-6">
                              <section class="panel">
                                  <header class="panel-heading">
                                      Pie
                                  </header>
                                  <div class="panel-body text-center">
                                      <canvas id="pie" height="300" width="400"></canvas>
                                  </div>
                              </section>
                          </div>
                          <!-- Doughnut -->
                          <div class="col-lg-6">
                              <section class="panel">
                                  <header class="panel-heading">
                                      Doughnut
                                  </header>
                                  <div class="panel-body text-center">
                                      <canvas id="doughnut" height="300" width="400"></canvas>
                                  </div>
                              </section>
                          </div>
                      </div>
                  </div>
                    <div class="panel-heading">
              <h2><strong>Registered Users</strong></h2>
              <div class="panel-actions">
                <a href="index.html#" class="btn-setting"><i class="fa fa-rotate-right"></i></a>
                <a href="index.html#" class="btn-minimize"><i class="fa fa-chevron-up"></i></a>
                <a href="index.html#" class="btn-close"><i class="fa fa-times"></i></a>
              </div>
            </div>
         
              <table class="table bootstrap-datatable countries">
                <thead>
                  <tr>
                    <th>Country</th>
                    <th>Users</th>
                    <th>Online</th>
                    <th>Performance</th>
                  </tr>
                </thead>   
                <tbody>
                  <tr>
                    
                    <td>Germany</td>
                    <td>2563</td>
                    <td>1025</td>
                    <td>
                      <div class="progress thin">
                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="73" aria-valuemin="0" aria-valuemax="100" style="width: 73%">
                        </div>
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="27" aria-valuemin="0" aria-valuemax="100" style="width: 27%">
                          </div>
                      </div>
                      <span class="sr-only">73%</span>    
                    </td>
                  </tr>
                  <tr>
                   
                    <td>India</td>
                    <td>3652</td>
                    <td>2563</td>
                    <td>
                      <div class="progress thin">
                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="57" aria-valuemin="0" aria-valuemax="100" style="width: 57%">
                        </div>
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="43" aria-valuemin="0" aria-valuemax="100" style="width: 43%">
                        </div>
                      </div>
                      <span class="sr-only">57%</span>    
                    </td>
                  </tr>
                  <tr>
                    
                    <td>Spain</td>
                    <td>562</td>
                    <td>452</td>
                    <td>
                      <div class="progress thin">
                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="93" aria-valuemin="0" aria-valuemax="100" style="width: 93%">
                        </div>
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="7" aria-valuemin="0" aria-valuemax="100" style="width: 7%">
                          </div>
                      </div>
                      <span class="sr-only">93%</span>    
                    </td>
                  </tr>
                  <tr>
                   
                    <td>Russia</td>
                    <td>1258</td>
                    <td>958</td>
                    <td>
                      <div class="progress thin">
                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                          </div>
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                          </div>
                      </div>
                      <span class="sr-only">20%</span>    
                    </td>
                  </tr>
                  <tr>
                   
                    <td>USA</td>
                    <td>4856</td>
                    <td>3621</td>
                    <td>
                      <div class="progress thin">
                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                          </div>
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                          </div>
                      </div>
                      <span class="sr-only">20%</span>    
                    </td>
                  </tr>
                  <tr>
                   
                    <td>Brazil</td>
                    <td>265</td>
                    <td>102</td>
                    <td>
                      <div class="progress thin">
                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                          </div>
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                          </div>
                      </div>
                      <span class="sr-only">20%</span>    
                    </td>
                  </tr>
                  <tr>
                  
                    <td>Coloumbia</td>
                    <td>265</td>
                    <td>102</td>
                    <td>
                      <div class="progress thin">
                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                          </div>
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                          </div>
                      </div>
                      <span class="sr-only">20%</span>    
                    </td>
                  </tr>
                  <tr>
                   
                    <td>France</td>
                    <td>265</td>
                    <td>102</td>
                    <td>
                      <div class="progress thin">
                          <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                          </div>
                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                          </div>
                      </div>
                      <span class="sr-only">20%</span>    
                    </td>
                  </tr>
                </tbody>
              </table>

            </div>
    </div>
    @include('footer') {{-- Include footer file --}} 
</div>

    <script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/jquery-1.8.3.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/jquery.scrollTo.min.js') }}"></script>
    <script src="{{ asset('js/jquery.nicescroll.js') }}"></script>
    <script src="{{ asset('js/Chart.js') }}"></script>
    <script src="{{ asset('js/chartjs-custom.js') }}"></script>
    <script src="{{ asset('js/scripts.js') }}"></script>

  </body>
 </html>
@endsection

