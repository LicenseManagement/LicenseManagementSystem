@extends('layouts.app')
@section('content')
@include('header') {{-- Include header file --}} 
<div class="container">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-heading">Edite User</div>
                <div class="panel-body">
                <form class="col-md-6 col-md-offset-3" role="form" method="POST" action="{{URL::to('/view_user/update')}}">
                {{ csrf_field() }}
                        <div class="form-group row{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name *</label>
                            <div class="col-md-6">
                            <input id="name" type="text" class="form-control" name="name" value="{{$user->name}}" required autofocus>
                            </div>
                        </div>
                      <fieldset disabled>
                        <div class="form-group row{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="disabledTextInput" class="col-md-4 control-label">E-Mail Address </label>
                            <div class="col-md-6">
                            <input id="disabledTextInput" type="text" class="form-control" name="email" value="{{$user->email}}" required autofocus>
                            </div>
                        </div>
                        </fieldset>
                         <div class="form-group row">
                            <label for="Sex" class="col-md-4 control-label">Gender</label>
                            <div class="col-md-6">
                                 <select class="form-control" id="sex" name="gender">
                                  <option value="{{$user->gender}}">Female</option>
                                  <option value="{{$user->gender}}">Male</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="status" class="col-md-4 control-label">Status</label>
                            <div class="col-md-6">
                            <select class="form-control" id="status" name="status">
                                  <option value="0">Inactive</option>
                                  <option value="1">Active</option>
                            </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="role" class="col-md-4 control-label">Role</label>
                            <div class="col-md-6">
                             <select class="form-control" id="role" name="role_id">
                              <?php 
                                foreach ($roles as $key => $value) {
                                  $selected = '';
                                  if($value->roleid == $user->role_id){
                                    $selected = 'selected';
                                  }
                                  echo "<option value=".$value->roleid." ".$selected.">".$value->role_name."</option>";
                                }
                              ?>
                            </select>
                            </div>
                        </div> 
                        <?php
                          //var_dump($user); 
                        ?>
                        <div class="form-group row">
                            <label for="Position" class="col-md-4 control-label">Position</label>
                            <div class="col-md-6">
                            <select class="form-control" id="position" name="position_id">
                                  <?php 
                                    foreach ($positions as $key => $value) {
                                      $selected = '';
                                      if($value->positionid == $user->positionid){
                                        $selected = 'selected';
                                      }
                                    echo "<option value=".$value->positionid." ".$selected.">".$value->position_name."</option>";
                                  }
                              ?>
                            </select>
                            </div>
                        </div> 
                         <div class="form-group row">
                            <label for="province" class="col-md-4 control-label">Province</label>
                            <div class="col-md-6">
                          <select class="form-control" id="province" name="province_id">
                              <option value="{{$user->province}}">{{$user->province}}</option>
                              <?php 
                                    foreach ($provinces as $key => $value) {
                                      $selected = '';
                                      if($value->province_id == $user->province_id){
                                        $selected = 'selected';
                                      }
                                    echo "<option value=".$value->province_id." ".$selected.">".$value->province."</option>";
                                  }
                              ?>
                            </select>
                            </div>
                        </div>            
                         <input type="hidden" name="id" value="{{$user->id}}"/>

                        <div class="form-group row">
                            <div class="col-md-6 col-md-offset-4">
                                 <button type="submit" class="btn btn-primary">Save</button>
                            </div>
                        </div>
                       
                   </form>
                </div>

        </div>
    </div>
</div>
 @include('footer') {{-- Include footer file --}} 
@endsection
