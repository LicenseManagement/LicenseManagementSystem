@extends('layouts.app')
@section('content')
@include('header') {{-- Include header file --}}   
 <div class="container-fluid">
	 <div class="span8">
   <legend>Monthly Report</legend>
      <form action="{{route('monthly.report')}}" method="POST" accept-charset="UTF-8">
          {!! csrf_field() !!}
          <div class="row">
          <div class="col-6 col-sm-4">
            <div class="form-group row">
              <label for="typeoflicense" class="col-sm-4 col-form-label">Type Of License</label>
                  <div class="col-sm-6">
                      <select class="form-control" id="license_type" name="licensetype">
                        <option value="">Select License type...</option>
                        <?php
                          foreach ($licenseType as $key => $value) {
                            echo '<option value="'.$value->type_id.'">'.$value->type.'</option>';
                          }
                        ?>
                      </select>
                   </div>
                </div>

            <div class="form-group row">
              <label for="province" class="col-sm-4 col-form-label">Province</label>
                  <div class="col-sm-6">
                    <select class="form-control" id="province" name="province">
                      <option value="">Select Province...</option>
                        <?php
                          foreach ($provinces as $key => $value) {
                            echo '<option value="'.$value->province_id.'">'.$value->province.'</option>';
                          }
                        ?>
                    </select>
                  </div>
            </div>
            </div>
            <div class="col-6 col-sm-4">
            <div class="form-group row">
              <label for="startdate" class="col-sm-3 col-form-label">From Date</label>
                <div class="col-sm-6">
                    <input type="text" name="start_date" class="form-control startdate" >
                 </div>
            </div>
            <div class="form-group row">
               <label for="enddate" class="col-sm-3 col-form-label">To Date</label>
                  <div class="col-sm-6">
                    <input type="text" name="end_date" class="form-control expiredate">
                  </div>
            </div>
            </div>
           <div class="col-6 col-sm-4">
            <div class="form-group row">
                <div class="col-md-5">
                    <input name="search" value="Search" class="btn btn-primary" type="submit">
                    <input class="btn btn-primary" name="excel" value="Excel" type="submit">
                </div>
            </div>
           </div>
      </div>
    </form>
    </div>
	 <div class="span10">
       <div class="row-fluid">
       <?php
          $newDataArr = array();
          foreach ($data as $key => $value) {
            // MOT block
            $newDataArr[$value->province]['MOT']['New']['T'] = 0;
            $newDataArr[$value->province]['MOT']['New']['R'] = 0;
            $newDataArr[$value->province]['MOT']['Renew']['T'] = 0;
            $newDataArr[$value->province]['MOT']['Renew']['R'] = 0;
            $newDataArr[$value->province]['MOT']['Suspended']['T'] = 0;
            $newDataArr[$value->province]['MOT']['Suspended']['R'] = 0;
            $newDataArr[$value->province]['MOT']['Fine']['T'] = 0;
            $newDataArr[$value->province]['MOT']['Fine']['R'] = 0;
            $newDataArr[$value->province]['MOT']['Closed']['T'] = 0;
            $newDataArr[$value->province]['MOT']['Closed']['R'] = 0;
            $newDataArr[$value->province]['MOT']['Nolicense']['T'] = 0;
            $newDataArr[$value->province]['MOT']['Nolicense']['R'] = 0;
            // End of MOT block
            // DOT block
            $newDataArr[$value->province]['DOT']['New']['T'] = 0;
            $newDataArr[$value->province]['DOT']['New']['R'] = 0;
            $newDataArr[$value->province]['DOT']['Renew']['T'] = 0;
            $newDataArr[$value->province]['DOT']['Renew']['R'] = 0;
            $newDataArr[$value->province]['DOT']['Suspended']['T'] = 0;
            $newDataArr[$value->province]['DOT']['Suspended']['R'] = 0;
            $newDataArr[$value->province]['DOT']['Fine']['T'] = 0;
            $newDataArr[$value->province]['DOT']['Fine']['R'] = 0;
            $newDataArr[$value->province]['DOT']['Closed']['T'] = 0;
            $newDataArr[$value->province]['DOT']['Closed']['R'] = 0;
            $newDataArr[$value->province]['DOT']['Nolicense']['T'] = 0;
            $newDataArr[$value->province]['DOT']['Nolicense']['R'] = 0;
            // End of DOT block
            // OWSO block
            $newDataArr[$value->province]['OWSO']['New']['T'] = 0;
            $newDataArr[$value->province]['OWSO']['New']['R'] = 0;
            $newDataArr[$value->province]['OWSO']['Renew']['T'] = 0;
            $newDataArr[$value->province]['OWSO']['Renew']['R'] = 0;
            $newDataArr[$value->province]['OWSO']['Suspended']['T'] = 0;
            $newDataArr[$value->province]['OWSO']['Suspended']['R'] = 0;
            $newDataArr[$value->province]['OWSO']['Fine']['T'] = 0;
            $newDataArr[$value->province]['OWSO']['Fine']['R'] = 0;
            $newDataArr[$value->province]['OWSO']['Closed']['T'] = 0;
            $newDataArr[$value->province]['OWSO']['Closed']['R'] = 0;
            $newDataArr[$value->province]['OWSO']['Nolicense']['T'] = 0;
            $newDataArr[$value->province]['OWSO']['Nolicense']['R'] = 0;
            // End of OWSO block
          }
          foreach ($data as $key => $value) {

            $t = 0;
            $r = 0;
            if($value->total != null)
              $t = $value->total;
            if($value->total_room != null)
              $r = $value->total_room;
            $newDataArr[$value->province][$value->approved_by][$value->typeofrequest]['T'] = $t;
            $newDataArr[$value->province][$value->approved_by][$value->typeofrequest]['R'] = $r;
            /*var_dump($newDataArr);
            die();*/
          }
          /*$newDataArr = ($arrTem+$newDataArr);*/
        
       ?>
         <div class="span12">
         	  <!-- Portlet: Member List -->
              <div class="box" id="box-0" style="overflow: auto;">
                       
               <div>
                   <div class="box-content">
                      <table cellpadding="0" cellspacing="0" border="0" style="margin: 0;" class="table table-striped table-bordered bootstrap-datatable" id="datatable">
                           <thead class="b tcenter">
                              <tr>
                                <td style="text-align: center;" colspan="40">Hotel</td>
                              </tr>
                             <tr style="text-align: center;">
                             <td rowspan="4" style="text-align: center; vertical-align: middle;">No</td>
                              <td rowspan="4" style="text-align: center; vertical-align: middle;">Location</td>
                             </tr>
                             <tr>
                              {{--Start Cash In block--}}
                               <td colspan="12" style="text-align: center;">MOT</td>
                               <td colspan="12" style="text-align: center;">DOT</td>
                               <td colspan="12" style="text-align: center;">OWSO</td>
                               <td rowspan="2" colspan="2" style="text-align: center; vertical-align: middle;">Total</td>
                              {{--End Cashin block--}}
                             </tr>
                             <tr>
                              {{--Start Cash In block--}}
                               <td colspan="2" style="text-align: center;">New</td>
                               <td colspan="2" style="text-align: center;">Renew</td>
                               <td colspan="2" style="text-align: center;">Supend</td>
                               <td colspan="2" style="text-align: center;">Fine</td>
                               <td colspan="2" style="text-align: center;">Close</td>
                               <td colspan="2" style="text-align: center;">No Licence</td>

                               <td colspan="2" style="text-align: center;">New</td>
                               <td colspan="2" style="text-align: center;">Renew</td>
                               <td colspan="2" style="text-align: center;">Supend</td>
                               <td colspan="2" style="text-align: center;">Fine</td>
                               <td colspan="2" style="text-align: center;">Close</td>
                               <td colspan="2" style="text-align: center;">No Licence</td>

                               <td colspan="2" style="text-align: center;">New</td>
                               <td colspan="2" style="text-align: center;">Renew</td>
                               <td colspan="2" style="text-align: center;">Supend</td>
                               <td colspan="2" style="text-align: center;">Fine</td>
                               <td colspan="2" style="text-align: center;">Close</td>
                               <td colspan="2" style="text-align: center;">No Licence</td>

                              {{--End Cashin block--}}
                             </tr>
                             <tr>
                              {{--Start Cash In block--}}
                               <td>Number</td>
                               <td>Rooms</td>
                               <td>Number</td>
                               <td>Rooms</td>
                               <td>Number</td>
                               <td>Rooms</td>
                               <td>Number</td>
                               <td>Rooms</td>
                               <td>Number</td>
                               <td>Rooms</td>
                               <td>Number</td>
                               <td>Rooms</td>

                               <td>Number</td>
                               <td>Rooms</td>
                               <td>Number</td>
                               <td>Rooms</td>
                               <td>Number</td>
                               <td>Rooms</td>
                               <td>Number</td>
                               <td>Rooms</td>
                               <td>Number</td>
                               <td>Rooms</td>
                               <td>Number</td>
                               <td>Rooms</td>

                               <td>Number</td>
                               <td>Rooms</td>
                               <td>Number</td>
                               <td>Rooms</td>
                               <td>Number</td>
                               <td>Rooms</td>
                               <td>Number</td>
                               <td>Rooms</td>
                               <td>Number</td>
                               <td>Rooms</td>
                               <td>Number</td>
                               <td>Rooms</td>

                               <td>Number</td>
                               <td>Rooms</td>
                              {{--End Cashin block--}}
                             </tr>
                             </thead> 
                           <tbody>
                             <?php
                              $i = 1;
                              // MOT
                              $totalMNewT = 0;
                              $totalMNewR = 0;
                              $totalMRenewT = 0;
                              $totalMRenewR = 0;
                              $totalMSuspendedT = 0;
                              $totalMSuspendedR = 0;
                              $totalMFineT = 0;
                              $totalMFineR = 0;
                              $totalMClosedT = 0;
                              $totalMClosedR = 0;
                              $totalMNolicenseT = 0;
                              $totalMNolicenseR = 0;
                              // DOT
                              $totalDNewT = 0;
                              $totalDNewR = 0;
                              $totalDRenewT = 0;
                              $totalDRenewR = 0;
                              $totalDSuspendedT = 0;
                              $totalDSuspendedR = 0;
                              $totalDFineT = 0;
                              $totalDFineR = 0;
                              $totalDClosedT = 0;
                              $totalDClosedR = 0;
                              $totalDNolicenseT = 0;
                              $totalDNolicenseR = 0;
                              // OWSO
                              $totalONewT = 0;
                              $totalONewR = 0;
                              $totalORenewT = 0;
                              $totalORenewR = 0;
                              $totalOSuspendedT = 0;
                              $totalOSuspendedR = 0;
                              $totalOFineT = 0;
                              $totalOFineR = 0;
                              $totalOClosedT = 0;
                              $totalOClosedR = 0;
                              $totalONolicenseT = 0;
                              $totalONolicenseR = 0;

                              $allTotalT = 0;
                              $allTotalR = 0;
                              foreach ($newDataArr as $keys => $values) {
                                $total = 0;
                                $totalRoom = 0;
                            ?>
                              <tr>
                                <td><?php echo ($i);?></td>
                                <td><?php echo ($keys);?></td>
                                <?php
                                  foreach ($values as $key => $value) {
                                    if($key == 'MOT'){
                                      foreach ($value as $k => $val) {
                                        if($val['T'] > 0)
                                          $total = $total+$val['T'];
                                        if($val['R'] > 0)
                                          $totalRoom = $totalRoom+$val['R'];

                                        if($k == 'New'){
                                          echo "<td>".$val['T']."</td>";
                                          echo "<td>".$val['R']."</td>";
                                          $totalMNewT = $totalMNewT+$val['T'];
                                          $totalMNewR = $totalMNewR+$val['R'];
                                        }elseif ($k == 'Renew') {
                                          echo "<td>".$val['T']."</td>";
                                          echo "<td>".$val['R']."</td>";
                                          $totalMRenewT = $totalMRenewT+$val['T'];
                                          $totalMRenewR = $totalMRenewR+$val['R'];
                                        }elseif ($k == 'Suspended') {
                                          echo "<td>".$val['T']."</td>";
                                          echo "<td>".$val['R']."</td>";
                                          $totalMSuspendedT = $totalMSuspendedT+$val['T'];
                                          $totalMSuspendedR = $totalMSuspendedR+$val['R'];
                                        }elseif ($k == 'Fine') {
                                          echo "<td>".$val['T']."</td>";
                                          echo "<td>".$val['R']."</td>";
                                          $totalMFineT = $totalMFineT+$val['T'];
                                          $totalMFineR = $totalMFineR+$val['R'];
                                        }elseif ($k == 'Closed') {
                                          echo "<td>".$val['T']."</td>";
                                          echo "<td>".$val['R']."</td>";
                                          $totalMClosedT = $totalMClosedT+$val['T'];
                                          $totalMClosedR = $totalMClosedR+$val['R'];
                                        }elseif ($k == 'Nolicense') {
                                          echo "<td>".$val['T']."</td>";
                                          echo "<td>".$val['R']."</td>";
                                          $totalMNolicenseT = $totalMNolicenseT+$val['T'];
                                          $totalMNolicenseR = $totalMNolicenseR+$val['R'];
                                        }
                                      }
                                    }elseif($key == 'DOT'){
                                      foreach ($value as $k => $val) {
                                        if($val['T'] > 0)
                                          $total = $total+$val['T'];
                                        if($val['R'] > 0)
                                          $totalRoom = $totalRoom+$val['R'];

                                        if($k == 'New'){
                                          echo "<td>".$val['T']."</td>";
                                          echo "<td>".$val['R']."</td>";
                                          $totalDNewT = $totalDNewT+$val['T'];
                                          $totalDNewR = $totalDNewR+$val['R'];
                                        }elseif ($k == 'Renew') {
                                          echo "<td>".$val['T']."</td>";
                                          echo "<td>".$val['R']."</td>";
                                          $totalDRenewT = $totalDRenewT+$val['T'];
                                          $totaldRenewR = $totalDRenewR+$val['R'];
                                        }elseif ($k == 'Suspended') {
                                          echo "<td>".$val['T']."</td>";
                                          echo "<td>".$val['R']."</td>";
                                          $totalDSuspendedT = $totalDSuspendedT+$val['T'];
                                          $totalDSuspendedR = $totalDSuspendedR+$val['R'];
                                        }elseif ($k == 'Fine') {
                                          echo "<td>".$val['T']."</td>";
                                          echo "<td>".$val['R']."</td>";
                                          $totalDFineT = $totalDFineT+$val['T'];
                                          $totalDFineR = $totalDFineR+$val['R'];
                                        }elseif ($k == 'Closed') {
                                          echo "<td>".$val['T']."</td>";
                                          echo "<td>".$val['R']."</td>";
                                          $totalDClosedT = $totalDClosedT+$val['T'];
                                          $totalDClosedR = $totalDClosedR+$val['R'];
                                        }elseif ($k == 'Nolicense') {
                                          echo "<td>".$val['T']."</td>";
                                          echo "<td>".$val['R']."</td>";
                                          $totalDNolicenseT = $totalDNolicenseT+$val['T'];
                                          $totalDNolicenseR = $totalDNolicenseR+$val['R'];
                                        }
                                      }
                                    }elseif($key == 'OWSO'){
                                      foreach ($value as $k => $val) {
                                        if($val['T'] > 0)
                                          $total = $total+$val['T'];
                                        if($val['R'] > 0)
                                          $totalRoom = $totalRoom+$val['R'];

                                        if($k == 'New'){
                                          echo "<td>".$val['T']."</td>";
                                          echo "<td>".$val['R']."</td>";
                                          $totalONewT = $totalONewT+$val['T'];
                                          $totalONewR = $totalONewR+$val['R'];
                                        }elseif ($k == 'Renew') {
                                          echo "<td>".$val['T']."</td>";
                                          echo "<td>".$val['R']."</td>";
                                          $totalORenewT = $totalORenewT+$val['T'];
                                          $totalORenewR = $totalORenewR+$val['R'];
                                        }elseif ($k == 'Suspended') {
                                          echo "<td>".$val['T']."</td>";
                                          echo "<td>".$val['R']."</td>";
                                          $totalOSuspendedT = $totalOSuspendedT+$val['T'];
                                          $totalOSuspendedR = $totalOSuspendedR+$val['R'];
                                        }elseif ($k == 'Fine') {
                                          echo "<td>".$val['T']."</td>";
                                          echo "<td>".$val['R']."</td>";
                                          $totalOFineT = $totalOFineT+$val['T'];
                                          $totalOFineR = $totalOFineR+$val['R'];
                                        }elseif ($k == 'Closed') {
                                          echo "<td>".$val['T']."</td>";
                                          echo "<td>".$val['R']."</td>";
                                          $totalOClosedT = $totalOClosedT+$val['T'];
                                          $totalOClosedR = $totalOClosedR+$val['R'];
                                        }elseif ($k == 'Nolicense') {
                                          echo "<td>".$val['T']."</td>";
                                          echo "<td>".$val['R']."</td>";
                                          $totalONolicenseT = $totalONolicenseT+$val['T'];
                                          $totalONolicenseR = $totalONolicenseR+$val['R'];
                                        }
                                      }
                                    }
                                  }
                                  echo "<td>".$total."</td>";
                                  echo "<td>".$totalRoom."</td>"; 
                                  $allTotalT = $allTotalT+$total;
                                  $allTotalR = $allTotalR+$totalRoom;
                                ?>
                              </tr>
                            <?php    
                                $i++; 
                               } 
                             ?>
                             <tr>
                               <td colspan="2"><b>Sub Total</b></td>
                               <td><?php echo $totalMNewT;?></td>
                               <td><?php echo $totalMNewR;?></td>
                               <td><?php echo $totalMRenewT;?></td>
                               <td><?php echo $totalMRenewR;?></td>
                               <td><?php echo $totalMSuspendedT;?></td>
                               <td><?php echo $totalMSuspendedR;?></td>
                               <td><?php echo $totalMFineT;?></td>
                               <td><?php echo $totalMFineR;?></td>
                               <td><?php echo $totalMClosedT;?></td>
                               <td><?php echo $totalMClosedR;?></td>
                               <td><?php echo $totalMNolicenseT;?></td>
                               <td><?php echo $totalMNolicenseR;?></td>
                               <td><?php echo $totalDNewT;?></td>
                               <td><?php echo $totalDNewR;?></td>
                               <td><?php echo $totalDRenewT;?></td>
                               <td><?php echo $totalDRenewR;?></td>
                               <td><?php echo $totalDSuspendedT;?></td>
                               <td><?php echo $totalDSuspendedR;?></td>
                               <td><?php echo $totalDFineT;?></td>
                               <td><?php echo $totalDFineR;?></td>
                               <td><?php echo $totalDClosedT;?></td>
                               <td><?php echo $totalDClosedR;?></td>
                               <td><?php echo $totalDNolicenseT;?></td>
                               <td><?php echo $totalDNolicenseR;?></td>
                               <td><?php echo $totalONewT;?></td>
                               <td><?php echo $totalONewR;?></td>
                               <td><?php echo $totalORenewT;?></td>
                               <td><?php echo $totalORenewR;?></td>
                               <td><?php echo $totalOSuspendedT;?></td>
                               <td><?php echo $totalOSuspendedR;?></td>
                               <td><?php echo $totalOFineT;?></td>
                               <td><?php echo $totalOFineR;?></td>
                               <td><?php echo $totalOClosedT;?></td>
                               <td><?php echo $totalOClosedR;?></td>
                               <td><?php echo $totalONolicenseT;?></td>
                               <td><?php echo $totalONolicenseR;?></td>
                               <td><?php echo $allTotalT;?></td>
                               <td><?php echo $allTotalR;?></td>
                             </tr>
                           </tbody>
                       </table>           
                   </div>
               </div>
             </div><!--/span-->
          </div>
  </div>
</div> 
</div>   

@include('footer') {{-- Include footer file --}} 
@endsection
@section('script')
<script type="text/javascript">
  $('.startdate').datepicker({

    dateFormat: "yy-mm-dd"

    // dateFormat: "yy-mm-dd"
  });
  $('.expiredate').datepicker({
    dateFormat: "yy-mm-dd"
  });
</script>
@endsection