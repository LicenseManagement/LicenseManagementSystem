<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Typeoflicense;


class TypeoflicenseController extends Controller
{
    //
    public function index()
    {
         $typeoflicense = DB::table('tbl_typeoflicense')->paginate(10);
         return view('layouts.pages.typeoflicense', ['tbl_typeoflicense' => $typeoflicense]);
    }
    public function store(Request $request)
    {
          $typeoflicense = new Typeoflicense;
          $typeoflicense->type = $request->ltype;
          $typeoflicense->description = $request->linfo;
          $typeoflicense->save();

          return redirect()->back();
    }

     public function show()
    {
           
          $typeoflicense = DB::table('tbl_typeoflicense')->get();
         return view('layouts.pages.typeoflicense', ['tbl_typeoflicense' => $typeoflicense]);
    }
    
}