<?php
// config for change password
use Illuminate\Support\Facades\Input as Input;
use App\User;
// end config change password
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/dashboard');
});
Route::get('/login', function () {
    return view('auth.login');
});
Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('change/password', function() {return view('auth.change_password'); });

Route::post('change/password', 'Auth\UpdatePasswordController@update');

Route::get('/dashboard', 'DashboardController@index');
Route::get('/managelicense', 'ManageLicenseController@index');
Route::get('/manageuser', 'ManageuserControler@index');


Route::group(['middleware' => ['auth']],function(){

Route::get('/add_new_user', ['uses' => 'Auth\RegisterController@index','as' => 'add_new_user']);	
Route::post('/add_new_user', array( 'uses' => 'Auth\RegisterController@store','as' => 'register.new_user'));


Route::get('/add_new_user', ['uses' => 'Auth\RegisterController@index','as' => 'add_new_user']);	
Route::post('/add_new_user', array( 'uses' => 'Auth\RegisterController@store','as' => 'register.new_user'));
});
Route::get('/view_user', 'ViewuserController@index');
Route::get('/view_user/edit/{id}', 'ViewuserController@edit');
Route::post('/view_user/update', 'ViewuserController@update');

Route::get('/national', ['uses' => 'NationalController@index', 'as' => 'national']);
Route::post('/national',['uses' => 'NationalController@store','as' => 'national.stored'] );
Route::get('/national/show', 'NationalController@show');
Route::get('/edit_national/{national_id}', 'NationalController@edit');
Route::post('/update_national/{national_id}', 'NationalController@update');
Route::post('/delete_national/{national_id}', 'NationalController@delete');
Route::post('/national/search_national', 'NationalController@search_national');

Route::get('/typeoflicense', 'TypeoflicenseController@index');
Route::post('/typeoflicense',['uses' => 'TypeoflicenseController@store','as' => 'typeoflicense.stored']);
Route::get('/typeoflicense/show', 'TypeoflicenseController@show');

Route::get('/province', 'ProvinceController@index');
Route::post('/province',['uses' => 'ProvinceController@store','as' => 'province.stored'] );
Route::get('/province/show', 'ProvinceController@show');

Route::get('/report', 'ReportControler@index');

Route::get('/addlicense', 'AddlicenseController@index');
Route::post('/addlicense', ['uses' => 'AddlicenseController@store', 'as' => 'add.license']);
Route::get('/addlicense/show', 'AddlicenseController@show');

Route::get('/viewlicense', 'AddlicenseController@show');

Route::get('/expired_license', 'ViewlicenseController@expiredlicense');
Route::get('/expired_license/edit/{license_id}', 'ViewlicenseController@EditLicenseExpired');
Route::post('/expired_license/update', 'ViewlicenseController@updateExpiredLicense');

Route::get('/view_license_detail/{license_id}', 'ViewlicenseController@view_detail');
Route::get('/edit_license/{license_id}', 'ViewlicenseController@edit_license');



Route::get('/monthly_report', 'ReportController@monthlyReport');
Route::get('/report/monthly', 'ReportController@monthlyReport');
Route::post('/report/monthly', ['uses' => 'ReportController@monthlyReport', 'as' => 'monthly.report']);
Route::get('/report/annual', ['uses' => 'ReportController@annualReport', 'as' => 'annual.report']);
Route::post('/report/annual', ['uses' => 'ReportController@annualReport', 'as' => 'annual.report']);


Route::get('/addlicense', 'AddlicenseController@index');
Route::post('/addlicense', ['uses' => 'AddlicenseController@store', 'as' => 'add.license']);

Route::get('/addlicense/show', 'AddlicenseController@show');

Route::get('/viewlicense', 'AddlicenseController@show');