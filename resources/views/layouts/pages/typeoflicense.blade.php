@extends('layouts.app')
@section('content')  
@include('header') {{-- Include header file --}} 

<div class="container">

    <div class="row">
        
            <div class="panel panel-default">
                <div class="panel-heading">List all Type Of License</div>
                <div class="panel-body">
                   

                    <button type="button" class="btn btn-info" data-toggle="collapse" 
                        data-target="#demo">Add Type Of License</button>

                        <form action="{{route('typeoflicense.stored')}}" method="POST">
                      {!! csrf_field() !!}
                          <div class="col-xs-6">

                            <div id="demo" class="collapse" class="form-group">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">License Type</label>
                                    <input type="text" name="ltype" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                                </div>
                              
                                <div class="form-group">
                                    <label for="exampleTextarea">Description</label>
                                    <textarea class="form-control" name="linfo" id="exampleTextarea" rows="3"></textarea>
                                </div> 
                              <button type="submit" class="btn btn-primary">Save</button>
                              <button type="submit" class="btn btn-primary">Cancel</button>
                            </div>
                          </div>  
                        </form>
                          <form class="navbar-form navbar-left">
                              <div class="form-group">
                                <input type="text" class="form-control" placeholder="Search typeoflicense.....">
                              </div>
                              <button type="submit" class="btn btn-default">Go</button>
                        </form>

                      <div class="box-container-toggle">
                          <div class="box-content">

                            <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered bootstrap-datatable" id="datatable">
                           <tbody>
                              <tr>
                                <th>id</th>

                                <th>Type Of License</th>
                                
                                <th>Description</th>
                                <th>Action</th>
                               
                              </tr>


                              <?php
                                  foreach ($tbl_typeoflicense as $typeoflicense) {
                               ?>
                                 <td class="center"><?php echo $typeoflicense->type_id;?></td>
                                 <td class="center"><?php echo $typeoflicense->type;?></td>
                                 <td class="center"><?php echo $typeoflicense->description;?></td>
                                
                                 <td class="center">
                                    <a class="btn btn-info" href="edit" rel="tooltip" title="Edit"><i class="icon-edit icon-white"></i>  
                                        Edit </a>

                                    <a class="btn btn-danger" href="#" rel="tooltip" title="Delete">
                                         <i class="icon-trash icon-white"></i>  
                                        Delete                                            
                                     </a>
                                </td>
                              </tr>
                              <?php 
                                }
                              ?>


                           </tbody>
                        </table>                 
                        {{ $tbl_typeoflicense->links() }} 
                     </div>
                     </div>

                </div>
               
            </div>
      
    </div>
   
</div>
 @include('footer') {{-- Include footer file --}} 
@endsection
