<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class ViewuserController extends Controller
{
    //
    public function index()
    {
 	$users = DB::table('users')
                ->join('tbl_province', 'tbl_province.province_id', '=', 'users.province_id')
                ->join('tbl_role', 'roleid', '=', 'role_id')
                ->orderBy('users.created_at', 'DESC')
                ->paginate(10);
    	return view('layouts.pages.view_user', ['users' => $users]);
    }
    
    public function edit($id){
        $user = DB::table('users')
                ->join('tbl_province', 'tbl_province.province_id', '=', 'users.province_id')
                ->join('tbl_role', 'roleid', '=', 'role_id')
                ->join('tbl_position','tbl_position.positionid','=','users.position_id')
                ->where('id',$id)
                ->first(); 
        $role = DB::table('tbl_role')->get();
        $positions = DB::table('tbl_position')->get();
        $provinces = DB::table('tbl_province')->get();
        return view('layouts.pages.edit_user', ['user' => $user,
                                                'roles'=>$role, 
                                                'positions' => $positions, 
                                                'provinces' => $provinces]);
    }
    public function update(Request $request){
        $input = $request->all();
        $id = $input['id'];
        unset($input['_token']);
        $user = DB::table('users')->where('id',$id)->update($input);       
        return redirect::to('/view_user');
    }
}