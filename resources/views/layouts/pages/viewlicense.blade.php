@extends('layouts.app')
@section('content')
@include('header') {{-- Include header file --}} 

<div class="container-fluid">
   <div class="row-fluid">
        <div class="panel panel-default">
            <div class="panel-heading">List all License</div>
                <div class="panel-body" >
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered bootstrap-datatable" id="datatable">
                        <thead>
                               <tr>
                                  <th>No</th>
                                   <th>Owner Name</th>
                                   <!-- <th>Sex</th> -->
                                   <!-- <th>Nationality</th> -->
                                   <th>Type Of Request</th>
                                   <th>Type Of License </th>
                                   <th>Approved By </th>
                                   <th>Name Of License</th>
                                   <!-- <th>Address </th> -->
                                   <th>Provinces</th>
                                   <th>Rooms </th>
                                   <th>Chairs</th>
                                   <th>Table </th>
                                   <th>Started Date </th>
                                   <th>Expired Date</th>
                                   <!-- <th>Established Year </th> -->
                                   <!-- <th>Telephone</th>
                                   <th>Total Staffs </th>
                                   <th>Women Staffs</th>
                                   <th>Foreign Staffs </th>
                                   <th>Document</th> -->
                                   <th>Action</th>
   
                               </tr>
                          </thead>   
                            <tbody>
                             <tr>
                              <?php
                                  $i = 1;
                                  foreach ($licenses as $license) {
                               ?>
                                 <td class="center"><?php echo $i;?></td>                                 
                                 <td class="center"><?php echo $license->ownername;?></td>

                                 <!-- <td class="center"><?php echo $license->sex;?></td>
                                 <td class="center"><?php echo $license->national_id;?> </td> -->

                                 <td class="center"><?php echo $license->type_request;?></td>
                                 <td class="center"><?php echo $license->type;?></td>

                                 <td class="center"><?php echo $license->approved_by;?></td>
                                 <td class="center"><?php echo $license->name_license;?> </td>

                                 <!-- <td class="center"><?php echo $license->address;?></td> -->

                                 <td class="center"><?php echo $license->province;?></td>
                                 <td class="center"><?php echo $license->room;?></td>
                                 <td class="center"><?php echo $license->chair;?></td>
                                 <td class="center"><?php echo $license->table;?> </td>
                                 <td class="center"><?php echo (date("d-m-Y", strtotime($license->start_date)));?></td>
                                 <td class="center"><?php echo (date("d-m-Y", strtotime($license->expired_date)));?> </td>
                                <!--  <td class="center"><?php echo $license->establishe_year;?> </td>
                                 <td class="center"><?php echo $license->telephone;?></td>
                                 <td class="center"><?php echo $license->total_staff;?></td>
                                 <td class="center"><?php echo $license->women_staff;?></td>
                                 <td class="center"><?php echo $license->foreign_staff;?></td>
                                 <td class="center"><?php echo $license->document;?> </td> -->

                                 <td class="center">
                                    <a class="btn btn-success" href="{{url('/view_license_detail/'.$license->license_id)}}" rel="tooltip" title="View">
                                         <span class="glyphicon glyphicon glyphicon-eye-open" aria-hidden="true">                                           
                                    </a>
                                    
                                    <a class="btn btn-danger pull-right" href="#" rel="tooltip" title="Delete">
                                         <span class="glyphicon glyphicon glyphicon-trash" aria-hidden="true">                                           
                                    </a>
                                    <a class="btn btn-info pull-right" href="{{url('/edit_license/'.$license->license_id)}}" rel="tooltip" title="Edit">
                                         <span class="glyphicon glyphicon glyphicon-pencil" aria-hidden="true">                                           
                                    </a>
                                </td>

                              </tr>
                              <?php 
                                $i++;
                                }
                              ?>
                           </tbody>
                </table>         
            </div>
        </div>
    </div>
</div>
@include('footer') {{-- Include footer file --}} 
@endsection
