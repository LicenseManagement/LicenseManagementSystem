@extends('layouts.app')
@section('content')
@include('header') {{-- Include header file --}}   
 <div class="container-fluid">
    <div class="span8">
      <legend>Annual Report</legend>
      <form action="{{route('annual.report')}}" method="POST" accept-charset="UTF-8">
      {!! csrf_field() !!}
        <div class="row">
            <div class="col-6 col-sm-4">
                <div class="form-group row">
                <label for="Sex" class="col-sm-3 col-form-label">From Date</label>
                  <div class="col-sm-6">
                      <input type="text" name="start_date" class="form-control startdate" >
                   </div>
                </div>
                <div class="form-group row">
               <label for="Sex" class="col-sm-3 col-form-label">To Date</label>
                  <div class="col-sm-6">
                    <input type="text" name="end_date" class="form-control expiredate">
                  </div>
            </div>
            </div>
            <div class="col-6 col-sm-4">
              <div class="form-group row">
                  <label for="Sex" class="col-sm-4 col-form-label">Type Of License</label>
                    <div class="col-sm-6">
                        <select class="form-control" id="license_type" name="licensetype">
                          <option value="">Select License type...</option>
                          
                          @foreach ($licensetypes as $licensetype) 
                            <option value="{{$licensetype->type_id}}">{{$licensetype->type}}</option>
                          @endforeach
                        
                        </select>
                     </div>
              </div>
             <div class="form-group row">
                  <div class="col-md-4">
                  </div>
                  <div class="col-md-3">
                      <input name="search" value="Search" class="btn btn-primary" type="submit">
                  </div>
                  <div class="col-md-3">
                     
                      <input class="btn btn-primary" name="excel" value="Excel" type="submit">
                  </div>
             </div>
          </div>
        </div>
      ​</form>
      </div>
      <?php
          $newDataArr = array();
          foreach ($data as $key => $value) {
            // MOT block
            $newDataArr[$value->province]['MOT']['New']['T'] = 0;
            $newDataArr[$value->province]['MOT']['New']['R'] = 0;
            $newDataArr[$value->province]['MOT']['Renew']['T'] = 0;
            $newDataArr[$value->province]['MOT']['Renew']['R'] = 0;
            $newDataArr[$value->province]['MOT']['Suspended']['T'] = 0;
            $newDataArr[$value->province]['MOT']['Suspended']['R'] = 0;
            $newDataArr[$value->province]['MOT']['Fine']['T'] = 0;
            $newDataArr[$value->province]['MOT']['Fine']['R'] = 0;
            $newDataArr[$value->province]['MOT']['Closed']['T'] = 0;
            $newDataArr[$value->province]['MOT']['Closed']['R'] = 0;
            $newDataArr[$value->province]['MOT']['Nolicense']['T'] = 0;
            $newDataArr[$value->province]['MOT']['Nolicense']['R'] = 0;
            // End of MOT block
            // DOT block
            $newDataArr[$value->province]['DOT']['New']['T'] = 0;
            $newDataArr[$value->province]['DOT']['New']['R'] = 0;
            $newDataArr[$value->province]['DOT']['Renew']['T'] = 0;
            $newDataArr[$value->province]['DOT']['Renew']['R'] = 0;
            $newDataArr[$value->province]['DOT']['Suspended']['T'] = 0;
            $newDataArr[$value->province]['DOT']['Suspended']['R'] = 0;
            $newDataArr[$value->province]['DOT']['Fine']['T'] = 0;
            $newDataArr[$value->province]['DOT']['Fine']['R'] = 0;
            $newDataArr[$value->province]['DOT']['Closed']['T'] = 0;
            $newDataArr[$value->province]['DOT']['Closed']['R'] = 0;
            $newDataArr[$value->province]['DOT']['Nolicense']['T'] = 0;
            $newDataArr[$value->province]['DOT']['Nolicense']['R'] = 0;
            // End of DOT block
            // OWSO block
            $newDataArr[$value->province]['OWSO']['New']['T'] = 0;
            $newDataArr[$value->province]['OWSO']['New']['R'] = 0;
            $newDataArr[$value->province]['OWSO']['Renew']['T'] = 0;
            $newDataArr[$value->province]['OWSO']['Renew']['R'] = 0;
            $newDataArr[$value->province]['OWSO']['Suspended']['T'] = 0;
            $newDataArr[$value->province]['OWSO']['Suspended']['R'] = 0;
            $newDataArr[$value->province]['OWSO']['Fine']['T'] = 0;
            $newDataArr[$value->province]['OWSO']['Fine']['R'] = 0;
            $newDataArr[$value->province]['OWSO']['Closed']['T'] = 0;
            $newDataArr[$value->province]['OWSO']['Closed']['R'] = 0;
            $newDataArr[$value->province]['OWSO']['Nolicense']['T'] = 0;
            $newDataArr[$value->province]['OWSO']['Nolicense']['R'] = 0;
            // End of OWSO block
          }
          foreach ($data as $key => $value) {

            $t = 0;
            $r = 0;
            if($value->total != null)
              $t = $value->total;
            if($value->total_room != null)
              $r = $value->total_room;
            $newDataArr[$value->province][$value->approved_by][$value->typeofrequest]['T'] = $t;
            $newDataArr[$value->province][$value->approved_by][$value->typeofrequest]['R'] = $r;
          }
       ?>
      <div class="box" id="box-0" style="overflow: auto;"> 
         <div class="box-content">
            <table cellpadding="0" cellspacing="0" border="0" style="margin: 0;" class="table table-striped table-bordered bootstrap-datatable" id="datatable">
                 <thead class="b tcenter">
                    <tr>
                    <td rowspan="4" style="text-align: center; vertical-align: middle;">No</td>
                    <td rowspan="4" style="text-align: center; vertical-align: middle;">Location</td>
                      <td style="text-align: center;" colspan="10">Hotel</td>
                      <td colspan="7" style="text-align: center"  >Total</td>
                    </tr>
                   <tr>
                     <td colspan="6" style="text-align: center; vertical-align: middle;">Approved By</td>
                     <td colspan="4" style="text-align: center; vertical-align: middle;">Type of Request</td>
                     <td colspan="3" style="text-align: center; vertical-align: middle;">Active License/ Processing</td>
                     <td colspan="4" style="text-align: center; vertical-align: middle;">Inactive License/ Unprocessing</td>
                   </tr>
                   <tr>
                    {{--Start Cash In block--}}
                     <td colspan="2" style="text-align: center;">MOT</td>
                     <td colspan="2" style="text-align: center;">DOT</td>
                     <td colspan="2" style="text-align: center;">OWSO</td>
                     <td colspan="2" style="text-align: center;">No License</td>
                     <td colspan="2" style="text-align: center;">Fine</td>
                     <td rowspan="2" style="text-align: center; vertical-align: middle;">Number</td>
                     <td rowspan="2" style="text-align: center; vertical-align: middle;">Rooms</td>
                     <td rowspan="2" style="text-align: center; vertical-align: middle;">Staff</td>
                     <td colspan="2" style="text-align: center;">Closed</td>
                     <td colspan="2" style="text-align: center;">Suspended</td>
                    {{--End Cashin block--}}
                   </tr>
                   <tr>
                    {{--Start Cash In block--}}
                     <td>Number</td>
                     <td>Rooms</td>

                     <td>Number</td>
                     <td>Rooms</td>
                     
                     <td>Number</td>
                     <td>Rooms</td>

                     <td>Number</td>
                     <td>Rooms</td>

                     <td>Number</td>
                     <td>Rooms</td>
      
                     <td>Number</td>
                     <td>Rooms</td>
                     <td>Number</td>
                     <td>Rooms</td>
                    {{--End Cashin block--}}
                   </tr>
                   </thead> 
                 <tbody>
                    <?php
                      $i = 1;
                      
                      $allTotalT = 0;
                      $allTotalR = 0;
                      $totalStaff = 0;

                      $totalAllMN = 0;
                      $totalAllMR = 0;

                      $totalAllDN = 0;
                      $totalAllDR = 0;

                      $totalAllON = 0;
                      $totalAllOR = 0;

                      $totalAllNN = 0;
                      $totalAllNR = 0;

                      $totalAllFN = 0;
                      $totalAllFR = 0;

                      $totalAllCN = 0;
                      $totalAllCR = 0;

                      $totalAllSN = 0;
                      $totalAllSR = 0;
                      foreach ($newDataArr as $keys => $values) {
                        // MOT
                        $totalMNewT = 0;
                        $totalMNewR = 0;
                        // DOT
                        $totalDNewT = 0;
                        $totalDNewR = 0;
                        // OWSO
                        $totalONewT = 0;
                        $totalONewR = 0;
                        $totalSuspendedT = 0;
                        $totalSuspendedR = 0;
                        $totalFineT = 0;
                        $totalFineR = 0;
                        $totalClosedT = 0;
                        $totalClosedR = 0;
                        $totalNolicenseT = 0;
                        $totalNolicenseR = 0;
                        $total = 0;
                        $totalRoom = 0;
                    ?>
                      <tr>
                        <td><?php echo ($i);?></td>
                        <td><?php echo ($keys);?></td>
                        <?php
                          foreach ($values as $key => $value) {
                            if($key == 'MOT'){
                              foreach ($value as $k => $val) {
                                if($k == 'New' || $k == 'Renew'){
                                  $totalMNewT = $totalMNewT+$val['T'];
                                  $totalMNewR = $totalMNewR+$val['R'];
                                }elseif ($k == 'Suspended') {
                                  $totalSuspendedT = $totalSuspendedT+$val['T'];
                                  $totalSuspendedR = $totalSuspendedR+$val['R'];
                                }elseif ($k == 'Fine') {
                                  $totalFineT = $totalFineT+$val['T'];
                                  $totalFineR = $totalFineR+$val['R'];
                                }elseif ($k == 'Closed') {
                                  $totalClosedT = $totalClosedT+$val['T'];
                                  $totalClosedR = $totalClosedR+$val['R'];
                                }elseif ($k == 'Nolicense') {
                                  $totalNolicenseT = $totalNolicenseT+$val['T'];
                                  $totalNolicenseR = $totalNolicenseR+$val['R'];
                                }
                              }
                            }elseif($key == 'DOT'){
                              foreach ($value as $k => $val) {
                                if($k == 'New' || $k == 'Renew'){
                                  $totalDNewT = $totalDNewT+$val['T'];
                                  $totalDNewR = $totalDNewR+$val['R'];
                                }elseif ($k == 'Suspended') {
                                  $totalSuspendedT = $totalSuspendedT+$val['T'];
                                  $totalSuspendedR = $totalSuspendedR+$val['R'];
                                }elseif ($k == 'Fine') {
                                  $totalFineT = $totalFineT+$val['T'];
                                  $totalFineR = $totalFineR+$val['R'];
                                }elseif ($k == 'Closed') {
                                  $totalClosedT = $totalClosedT+$val['T'];
                                  $totalClosedR = $totalClosedR+$val['R'];
                                }elseif ($k == 'Nolicense') {
                                  $totalNolicenseT = $totalNolicenseT+$val['T'];
                                  $totalNolicenseR = $totalNolicenseR+$val['R'];
                                }
                              }
                            }elseif($key == 'OWSO'){
                              foreach ($value as $k => $val) {
                                if($k == 'New' || $k == 'Renew'){
                                  $totalONewT = $totalONewT+$val['T'];
                                  $totalONewR = $totalONewR+$val['R'];
                                }elseif ($k == 'Suspended') {
                                  $totalSuspendedT = $totalSuspendedT+$val['T'];
                                  $totalSuspendedR = $totalSuspendedR+$val['R'];
                                }elseif ($k == 'Fine') {
                                  $totalFineT = $totalFineT+$val['T'];
                                  $totalFineR = $totalFineR+$val['R'];
                                }elseif ($k == 'Closed') {
                                  $totalClosedT = $totalClosedT+$val['T'];
                                  $totalClosedR = $totalClosedR+$val['R'];
                                }elseif ($k == 'Nolicense') {
                                  $totalNolicenseT = $totalNolicenseT+$val['T'];
                                  $totalNolicenseR = $totalNolicenseR+$val['R'];
                                }
                              }
                            }
                          }
                          $total = $totalMNewT+$totalDNewT+$totalONewT+$totalNolicenseT+$totalFineT;
                          $totalRoom = $totalMNewR+$totalDNewR+$totalONewR+$totalNolicenseR+$totalFineR;
                          echo "<td>".$totalMNewT."</td>";
                          echo "<td>".$totalMNewR."</td>";
                          echo "<td>".$totalDNewT."</td>";
                          echo "<td>".$totalDNewR."</td>";
                          echo "<td>".$totalONewT."</td>";
                          echo "<td>".$totalONewR."</td>";
                          echo "<td>".$totalNolicenseT."</td>";
                          echo "<td>".$totalNolicenseR."</td>";
                          echo "<td>".$totalFineT."</td>";
                          echo "<td>".$totalFineR."</td>";
                          echo "<td>".$total."</td>";
                          echo "<td>".$totalRoom."</td>";
                          echo "<td>0</td>";
                          echo "<td>".$totalClosedT."</td>";
                          echo "<td>".$totalClosedR."</td>";
                          echo "<td>".$totalSuspendedT."</td>";
                          echo "<td>".$totalSuspendedR."</td>";
                          
                          $totalAllMN = $totalAllMN+$totalMNewT;
                          $totalAllMR = $totalAllMR+$totalMNewR;
                          $totalAllDN = $totalAllDN+$totalDNewT;
                          $totalAllDR = $totalAllDR+$totalDNewR;
                          $totalAllON = $totalAllON+$totalONewT;
                          $totalAllOR = $totalAllOR+$totalONewR;
                          $totalAllNN = $totalAllNN+$totalNolicenseT;
                          $totalAllNR = $totalAllNR+$totalNolicenseR;
                          $totalAllFN = $totalAllFN+$totalFineT;
                          $totalAllFR = $totalAllFR+$totalFineR;
                          $allTotalT = $allTotalT+$total;
                          $allTotalR = $allTotalR+$totalRoom;
                          $totalStaff = 0;
                          $totalAllCN = $totalAllCN+$totalClosedT;
                          $totalAllCR = $totalAllCR+$totalClosedR;
                          $totalAllSN = $totalAllSN+$totalSuspendedT;
                          $totalAllSR = $totalAllSR+$totalSuspendedR;
                        ?>
                      </tr>
                    <?php    
                        $i++; 
                       } 
                     ?>
                   <tr>
                     <td colspan="2"><b>Sub Total</b></td>
                     <td><?php echo $totalAllMN;?></td>
                     <td><?php echo $totalAllMR;?></td>
                     <td><?php echo $totalAllDN;?></td>
                     <td><?php echo $totalAllDR;?></td>
                     <td><?php echo $totalAllON;?></td>
                     <td><?php echo $totalAllOR;?></td>
                     <td><?php echo $totalAllNN;?></td>
                     <td><?php echo $totalAllNR;?></td>
                     <td><?php echo $totalAllFN;?></td>
                     <td><?php echo $totalAllFR;?></td>
                     <td><?php echo $allTotalT;?></td>
                     <td><?php echo $allTotalR;?></td>
                     <td><?php echo $totalStaff;?></td>
                     <td><?php echo $totalAllCN;?></td>
                     <td><?php echo $totalAllCR;?></td>
                     <td><?php echo $totalAllSN;?></td>
                     <td><?php echo $totalAllSR;?></td>
                   </tr>
                 </tbody>
             </table>           
         </div>
      </div>
   </div>   
@include('footer') {{-- Include footer file --}} 
@endsection
@section('script')
<script type="text/javascript">
  $('.startdate').datepicker({

    dateFormat: "yy-mm-dd"

    // dateFormat: "yy-mm-dd"
  });
  $('.expiredate').datepicker({
    dateFormat: "yy-mm-dd"
  });
</script>
@endsection