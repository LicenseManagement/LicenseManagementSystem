<?php

namespace App\Http\Controllers\auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UpdatepasswordController extends Controller
{

    public function __construct() {

        $this->middleware('auth');

    }

    /**
     * Update the password for the user.
     *
     * @param  Request  $request
     * @return Response
     */
    public function update(Request $request)
    {


   // $this->validate($request, [
   //          'password' => 'required',
   //          'password_confirmation' => 'required|min:6|same:password',
   //      ]);

   //       $user_id = Auth::user()->id;
   //      $user = User::find($user_id);
   //      $user->password = Hash::make($request->password);
   //      $user->save();
   //      return redirect('change/password');



        $this->validate($request, [
            'old' => 'required',
            'password' => 'required',
            'password_confirmation' => 'required|min:6|same:password',
        ]);

        $user = User::find(Auth::id());

        $hashedPassword = $user->password;

        if (Hash::check($request->old, $hashedPassword)) {
            //Change the password
            $user->fill([
                'password' => Hash::make($request->password)
            ])->update();

            $request->session()->flash('success', 'Your password has been changed.');

            return back();
        }else{
             $request->session()->flash('failure', 'Your password has not been changed.');

        return back();
        }

       

	}
}
