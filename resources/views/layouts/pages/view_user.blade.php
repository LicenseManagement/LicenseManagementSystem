@extends('layouts.app')
@section('content')
@include('header') {{-- Include header file --}} 
<title>View User</title>            
   
<div class="container-fluid">
   <div class="row-fluid">
           
             <div class="panel panel-default">
              <div class="panel-heading">List all member</div>

                <div class="panel-body" >
                      <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered bootstrap-datatable" id="datatable">
                           <thead>
                               <tr>
                                  <th>No </th>
                                   <th>Full Name </th>
                                   <th>Email</th>
                                   <th>Gender </th>
                                   <th>Date registered </th>
                                   <th>Role </th>
                                   <th>Status </th>
                                   <th>Province</th>
                                   <th>Actions </th>
                               </tr>
                           </thead>   
                           <tbody>
                             <tr>
                              <?php
                                  $i = 0;
                                  foreach ($users as $user) {
                                    $i += 1;
                               ?>
                                 <td class="center"><?php echo $i;?></td>
                                 <td class="center"><?php echo $user->name;?></td>
                                 <td class="center"><?php echo $user->email;?></td>
                                 <td class="center"><?php echo $user->gender;?></td>
                                 <td class="center"><?php echo $user->created_at;?> </td>
                                 <td class="center"><?php echo $user->role_name;?></td>
                                 <td class="center">{{($user->status == 0 ? 'Inactive' : 'Active' )}}
                                 </td>
                                 <td class="center"><?php echo $user->province;?> </td>
                                 <td class="center">
                                     <a class="btn btn-info" href="{{URL::to('view_user/edit/'.$user->id)}}" rel="tooltip" title="Edit">
                                         <i class="icon-edit icon-white"></i>  
                                        Edit                                            
                                     </a>
                                </td>
                              </tr>
                              <?php 
                                }
                              ?>
                           </tbody>
                       </table> 
                       {{ $users->links() }}           
                   </div>
               </div>
            </div>
          </div>
@include('footer') 
@endsection