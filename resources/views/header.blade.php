<img src="{{url('/img/logo.png')}}" alt="Image"/ width="100%">
        <div class="col-md-2 login">
            <li class="dropdown" style="font-size:16px;">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                @if(Auth::check())
                {{ Auth::user()->name }} 
                @endif 
                    <span class="caret"></span>
                </a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="/change/password">Change Password</a>
                        </li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                                    Logout
                            </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                </form>
                        </li>
                    </ul>
            </li>
        </div>
            <ul class="nav nav-tabs" role="tablist" class="list-inline">
                <li class="dropdown">
                    <a href="/dashboard">Dashboard</a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        License Management <span class="caret"></span>
                    </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="/addlicense">Add New License</a>
                            </li>
                            <li>
                                <a href="/viewlicense">View License</a>
                            </li>
                       </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Categories Management <span class="caret"></span>
                    </a>
                <ul class="dropdown-menu" role="menu">
                    <li>
                        <a href="/national">National</a></li>
                    <li>
                        <a href="/typeoflicense">TypeOfLicense</a></li>
                    <li>
                        <a href="/province">Province</a></li>                               
                </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">User Management <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ route('add_new_user') }}">Add New User</a>
                        </li>
                        <li><a href="/view_user">View User</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Report <span class="caret"></span> </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="/report/monthly">Monthly</a></li>
                        <li><a href="/report/annual">Annual</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="/expired_license">License Expired</a>
                </li>
                              
        </ul>

